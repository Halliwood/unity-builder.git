import { CmdOption } from "./CmdOption.js";
import { BaseTask } from "./common/tasks/BastTask.js";
export declare type IAdditionalMsg = {
    type: 'text' | 'markdown';
    mention?: string;
    msg: string;
} | {
    type: 'file';
    file: string;
};
export declare abstract class Ancestor {
    protected cmdOption: CmdOption;
    protected series: BaseTask<any>[];
    constructor(cmdOption: CmdOption);
    abstract awake(): Promise<boolean>;
    abstract start(): Promise<void>;
    protected startSeries(): Promise<void>;
    getParams(): Promise<void>;
    private _getProjInfo;
    protected cleanProj(workspacePath: string, responsitory: string): Promise<void>;
    private readBuiltInTpl;
    getBuildConclusion(): Promise<string>;
    getAdditionalMessages(): Promise<IAdditionalMsg[]>;
}
