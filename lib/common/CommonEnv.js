export class CommonEnv {
    static PublishAssets = 'publish/assets';
    static EditorBuildSettingsAsset = 'ProjectSettings/EditorBuildSettings.asset';
    static TsSrc = 'TsScripts';
    static TsOutput = 'TsScripts/.dist';
    static PuertsWebGLJSLib = 'Assets/Puerts_webgl/Runtime/Plugins/WebGL';
    static PuertsDLLMockLibJS = 'Assets/Puerts_webgl/Javascripts~/PuertsDLLMock';
    static PuertsJSLib = 'Editor/Resources/puerts';
    static PuerGen = 'Assets/Gen';
    static PuertsWebglRoot = 'Assets/Puerts_webgl';
    static I18N_TS = 'TsScripts/System/i18n/i18n.ts';
    static I18N_CS = 'Assets/Scripts/i18n/i18n.cs';
    static RESLOADER_CS = 'Assets/Scripts/game/AssetLoader/ResLoader.cs';
    static Root_Unity = 'Assets/Scenes/root.unity';
    static Root_webgl_Unity = 'Assets/Scenes/root_webgl.unity';
    static ComLib = 'libs/cjs/com';
    static VersionCodeTxt = 'build/versionCode.txt';
}
//# sourceMappingURL=CommonEnv.js.map