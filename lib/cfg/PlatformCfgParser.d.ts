import { BuildParams } from "../BuildParams.js";
import { CmdOption } from "../CmdOption.js";
import { ChannelCfg } from "../typings";
export declare class PlatformCfgParser {
    private cfgs?;
    private cfgMap?;
    getChannelCfg(option: CmdOption, params: BuildParams): Promise<void>;
    getCfgByGameId(gameid: number): ChannelCfg | null;
    getAll(): ChannelCfg[] | undefined;
}
