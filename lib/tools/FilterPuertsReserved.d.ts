export declare class FilterPuertsReserved {
    private collection;
    readAll(projectRoot: string, output: string): Promise<string[]>;
    private readPuerWebGLJS;
    private readPuerDLLMockLibJS;
    private readPuerJS;
    private readPuerGen;
    readComLib(input: string, collection: string[]): Promise<void>;
    private readReservedTagged;
}
