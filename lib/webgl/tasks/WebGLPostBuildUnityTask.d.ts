import { BaseTask } from "../../common/tasks/BastTask.js";
export declare abstract class WebGLProcessJsTask extends BaseTask {
    protected processJSFiles(output: string, runEntry: 'buildForBrowser' | 'buildForMinigame'): Promise<void>;
}
