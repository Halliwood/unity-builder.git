import { BaseTask, TaskResult } from "./BastTask.js";
export declare class CheckCodeTask extends BaseTask {
    run(): Promise<TaskResult<void>>;
}
