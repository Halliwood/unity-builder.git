declare module 'tt-minigame-ide-cli' {
    export const setConfig: (
        config: { proxy: string; allowReportEvent?: boolean },
        useDefault?: boolean,
    ) => void;
    export const open: (options: {
        project: { path: string; mode?: 'full' | 'lite' };
    }) => Promise<void>;
    export const loginByEmail: (options: { email: string; password: string }) => Promise<void>;
    export const logout: () => Promise<void>;
    export const preview: (options: {
        project: {
            /**小游戏项目地址 */
            path: string;
        };
        page: {
            /**不需要填，但是必须要写上一个默认值 */
            path: '';
        };
        qrcode?: {
            /**以文件形式保存二维码 */
            format: 'imageFile';
            /**二维码保存地址 */
            output: string;
        };
        /**是否使用本地缓存获取二维码 */
        cache: boolean;
    }) => Promise<{
        shortUrl: string;
        expireTime: number;
        originSchema: string;
    }>;
    export const upload: (options: {
        project: {
            /**小游戏项目地址 */
            path: string;
        };
        qrcode?: {
            /**以文件形式保存二维码 */
            format: 'imageFile';
            /**二维码保存地址 */
            output: string;
        };
        /**此版本的版本日志 */
        changeLog: string;
        /**发布的小游戏版本 */
        version: string;
        channel: string;
        bgColor: string;
    }) => Promise<{
        shortUrl: string;
        expireTime: number;
        originSchema: string;
    }>;
    export const unity: {
        upload: (options: {
            /**项目路径 */
            projectPath: string;
            /**测试二维码存储到本地 */
            output: string;
            /**发布版本 */
            version: string;
            /**发布日志 */
            publishDesc: string;
            /**测试通道 */
            channel: string;
            /**输出的二维码图片背景色（16进制rgba格式） */
            bgColor: string;
        }) => Promise<{
            shortUrl: string;
            expireTime: number;
            originSchema: string;
        }>;
    }
}