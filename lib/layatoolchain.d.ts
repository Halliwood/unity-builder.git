import { ProjectType } from "./BuildParams.js";
import { TFileNameMapper, TMd5Map } from "./typings.js";
export declare interface ILayaBuildConfig {
    gameid: number;
    gamename: string;
    platid: number;
    pf: string;
    svrurl: string;
    svrips: string[];
    svripfmt: string;
    resurl: string;
    defines: string;
    version: string;
    fyReportUrl: string;
    uiReplacement?: {
        [original: string]: string;
    };
    statement: string;
    bottom: number;
}
interface LayaVars {
    publishRoot: string;
    buildConfigMap: {
        [pf: string]: ILayaBuildConfig;
    };
    fileNameMapper: TFileNameMapper;
    md5Map: TMd5Map;
    /**不同平台可能共享同一个CDN，共享一个patch包 */
    patchMap: {
        [file: string]: string;
    };
}
export declare const layatoolchain: {
    vars: LayaVars;
    commonFileRoot: () => string;
    projectFilesRoot: (pf: string, type: "common" | ProjectType) => string;
    newRecRoot: (oldToolsRoot: string) => string;
    projPublishRecRoot: (oldToolsRoot: string) => string;
    pfPublishRecRoot: (oldToolsRoot: string, pf: string) => string;
};
export {};
