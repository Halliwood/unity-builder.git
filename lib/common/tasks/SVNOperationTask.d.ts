import { BaseTask, TaskResult } from "./BastTask.js";
export interface SVNSumary {
    diffJsonNames?: string[];
    jsonDiff?: string;
    addNewCsharps?: string[];
}
export declare class SVNOperationTask extends BaseTask<SVNSumary> {
    run(): Promise<TaskResult<SVNSumary>>;
    protected cleanProj(workspacePath: string, responsitory: string): Promise<SVNSumary>;
}
