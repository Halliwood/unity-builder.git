import fs from 'fs';
import path from "path";
import { BaseTask, TaskResult } from "../../common/tasks/BastTask.js";
import { ignoreSVN } from '../../CommonAction.js';
import { toolchain } from '../../toolchain.js';
import { Cmd } from '../../tools/Cmd.js';
import { eitherPath } from '../../tools/vendor.js';
import { WebGLEnv } from "../WebGLEnv.js";

/**检查install */
export class WebGLInstallTask extends BaseTask<void> {
    async run(): Promise<TaskResult<void>> {
        const puertsWebglJSRoot = eitherPath(path.join(toolchain.params.workSpacePath, WebGLEnv.PuertsWebglJSbuild, 'Javascripts~'), 
        path.join(toolchain.params.workSpacePath, WebGLEnv.PuertsWebgl, 'Javascripts~'));
        const nodeModules = path.join(puertsWebglJSRoot, 'node_modules');
        if (!fs.existsSync(nodeModules)) {
            process.chdir(puertsWebglJSRoot);
            await new Cmd().runNodeModule('npm', ['install']);
            await ignoreSVN(puertsWebglJSRoot, ['node_modules']);
        }
        return { success: true, errorCode: 0, data: void 0 };
    }
}
