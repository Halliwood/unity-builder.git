import { CmdOption } from "../../CmdOption.js";
import { Nullable } from "../../typings";
export interface TaskResult<T = void> {
    success: boolean;
    errorCode: number;
    message?: string;
    data?: T;
}
export declare abstract class BaseTask<T = void> {
    protected cmdOption: CmdOption;
    constructor(cmdOption: CmdOption);
    abstract run(): Promise<TaskResult<T>>;
    /**是否永远执行，如为true则无视skip命令 */
    get executeAlways(): boolean;
    /**是否可跳过，注意只允许使用静态条件判断，不能使用项目配置等进行判断 */
    get skip(): boolean;
    /**依赖前置任务，被依赖的任务无视skip命令 */
    get dependencies(): Nullable<string[]>;
    get taskName(): string;
}
