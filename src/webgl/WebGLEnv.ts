import path from "path";

export class WebGLEnv {
    /**支持贴图压缩的unity最高版本 */
    public static readonly CompressTexMaxVer = '2022.3.14';

    public static readonly MD5Len = 6;
    public static readonly MangleBak = '../mangle_bak';
    public static readonly MangleReserved = '../mangle/manglereserved.txt';
    public static readonly MangleCache = '../mangle/manglecached.txt';
    public static readonly PuertsWebgl = 'Assets/Puerts_webgl';
    public static readonly PuertsWebglJSbuild = 'Assets/Puerts_webgl_jsbuild';
    public static readonly MiniGameConfigAsset = 'Assets/WX-WASM-SDK/Editor/MiniGameConfig.asset';
    public static readonly MiniGameConfigAssetV2 = 'Assets/WX-WASM-SDK-V2/Editor/MiniGameConfig.asset';
    public static readonly ExportbrowserRoot = '../export_browser';
    public static readonly ExportBuildRoot = 'Build';
    public static readonly SvrBrowserRoot = 'browser';
    public static readonly ExportMiniGameRoot = '../export_minigame';
    public static readonly MiniGameJsResources = 'puerts_minigame_js_resources';
    public static readonly MiniGameRoot = 'minigame';
    public static readonly MiniGameJs = 'game.js';
    public static readonly WebglRoot = 'webgl';
    public static readonly WebglCompressedOut = 'webgl-min';
    public static readonly MiniGameAssetRoot = 'assets/wx';
    public static readonly indexHtml = 'index.html';

    public static readonly PuertsRuntimeJs = 'puerts-runtime.js';
    public static readonly PuertsBrowserJsResourcesJs = 'puerts_browser_js_resources.js';

    public static readonly MiniGameAppVerFile = path.join(WebGLEnv.ExportMiniGameRoot, 'appVer.txt');
    public static readonly MiniGameResVerFile = path.join(WebGLEnv.ExportMiniGameRoot, 'resVer.txt');

    public static readonly MiniGameQrcode = path.join(WebGLEnv.ExportMiniGameRoot, 'qrcode.base64');
    public static readonly MiniGamePreviewOutput = path.join(WebGLEnv.ExportMiniGameRoot, 'preview.json');
    public static readonly MiniGameUploadOutput = path.join(WebGLEnv.ExportMiniGameRoot, 'upload.json');

    public static readonly ExportDouyinRoot = '../export_douyin';
    public static readonly DouyinJsRoot = 'Assets/StreamingAssets/__cp_js_files';
    public static readonly DouyinAppVerFile = path.join(WebGLEnv.ExportDouyinRoot, 'appVer.txt');
}
