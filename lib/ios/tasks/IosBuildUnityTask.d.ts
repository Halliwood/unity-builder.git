import { BaseTask, TaskResult } from "../../common//tasks/BastTask.js";
import { Nullable } from '../../typings';
export declare class IosBuildUnityTask extends BaseTask<void> {
    run(): Promise<TaskResult<void>>;
    private _exportIosProject;
    private fixXCodeProject;
    private _buildSignedIpa;
    private _signXCodeProject;
    private _copyIpa2UploadDir;
    get dependencies(): Nullable<string[]>;
    get skip(): boolean;
}
