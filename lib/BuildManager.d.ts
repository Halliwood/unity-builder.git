import { CmdOption } from "./CmdOption.js";
export declare class BuildManager {
    protected cmdOption: CmdOption;
    private executor?;
    constructor(cmdOption: CmdOption);
    start(): Promise<void>;
}
