import path from "path";
import { BaseTask } from "./BastTask.js";
import { toolchain } from "../../toolchain.js";
/**斗破专用提审任务，其他项目不建议使用 */
export class LegacyTiShenTask extends BaseTask {
    async run() {
        let uploadPath = toolchain.params.uploadPath;
        if (uploadPath[uploadPath.length - 1] != path.sep)
            uploadPath += path.sep;
        const params = ['-batchmode', '-projectPath', toolchain.params.workSpacePath, '-executeMethod', 'BuildCommand.SetTiShen',
            '-UploadResDir', uploadPath, '-platform', this.cmdOption.platform, '-private_obb', String(toolchain.params.BuildPrivateOBB || false),
            '-defines', 'PUBLISH', '-quit'];
        await toolchain.unity.runUnityCommand(this.cmdOption, params, 'SetTiShen', false);
        return { success: true, errorCode: 0 };
    }
}
//# sourceMappingURL=LegacyTiShenTask.js.map