import { BaseMakeHtmlTask } from "../../common/tasks/BaseMakeHtmlTask.js";
export class AndroidMakeHtmlTask extends BaseMakeHtmlTask {
    async run() {
        return await this.makeHtml('apk', ['*.apk', '*.aab']);
    }
}
//# sourceMappingURL=AndroidMakeHtmlTask.js.map