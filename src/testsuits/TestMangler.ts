import fs from "fs-extra";
import path from "path";
import { minify } from "terser";
import { Mangler } from "../tools/Mangler.js";

const testForMiniGame = async () => {
    console.time('TestMangler::testForMiniGame');
    const mangler = new Mangler();
    const reservedFile = 'F:/manglereserved.txt', nameCacheFile = 'F:/manglecache.json', jsonRoot ='D:/works/dp/trunk/project/Assets/AssetSources/data', jsRoot = 'D:/works/dp/trunk/project/TsScripts/.dist';
    const reservedContent = await fs.readFile(reservedFile, 'utf-8');
    const reserved = reservedContent.split(/\r?\n+/);
    await mangler.beginMangle(reserved, nameCacheFile, true, false);
    // await mangler.mangleAll(jsonRoot, jsonRoot, '.json');
    await mangler.mangleAll(jsRoot, jsRoot, '.js');
    await mangler.patchJs(jsRoot);
    await mangler.endMangle(nameCacheFile);
    console.timeEnd('TestMangler::testForMiniGame');
}
const testForBrowser = async () => {
    console.time('TestMangler::testForBrowser');
    const mangler = new Mangler();
    const reservedFile = 'F:/manglereserved.txt', nameCacheFile = 'F:/manglecache.json', jsRoot = 'D:/works/jsOut';
    const reservedContent = await fs.readFile(reservedFile, 'utf-8');
    const reserved = reservedContent.split(/\r?\n+/);
    await mangler.beginMangle(reserved, nameCacheFile, true, false);
    await mangler.mangleAll(jsRoot, jsRoot, '.js');
    const js = path.join(jsRoot, 'puerts_browser_js_resources.js');
    // await mangler.mangleOneJs(js, js);
    await mangler.patchJs(js);
    await mangler.endMangle(nameCacheFile);
    console.timeEnd('TestMangler::testForBrowser');
}
testForMiniGame();
// testForBrowser();
