import fs from 'fs';
import moment from 'moment';
import path from 'path';
import YAML from 'yaml';
import { toolchain } from '../toolchain.js';
export class SvnCfgParser {
    async getSVNCfg(option, params) {
        const svnCfgFile = path.join(option.oldToolsRoot, 'FYMGBuild/config/projSvn.yml');
        const hostInfoContent = fs.readFileSync(svnCfgFile, 'utf-8');
        const cfg = YAML.parse(hostInfoContent);
        let projCfg = cfg[params.projectName];
        if (!projCfg) {
            // 新项目不需要在projSvn.yml中配置svn信息了，直接从config.ini中读取
            const iCfg = await toolchain.projIniCfgParser.getProjIniCfg(params.projectName);
            if (iCfg) {
                projCfg = {
                    '代码': {
                        '前台': {
                            develop: {
                                '主路径': iCfg.Svn.ClientSvn.replace(/\/$/, ''),
                                tags: iCfg.Svn.ClientSvn.replace(/trunk\/?$/, 'tags/tag_')
                            },
                            publish: {
                                '主路径': iCfg.Svn.ClientSvn.replace(/trunk\/?$/, 'branches/publish'),
                                tags: iCfg.Svn.ClientSvn.replace(/trunk\/?$/, 'tags/publish_back_')
                            },
                            publishfix: {
                                '主路径': iCfg.Svn.ClientSvn.replace(/trunk\/?$/, 'branches/publishfix'),
                                tags: iCfg.Svn.ClientSvn.replace(/trunk\/?$/, 'tags/publishfix_back_')
                            },
                        },
                        '后台': iCfg.Svn.SvrSvn.replace(/\/trunk\/?$/, '')
                    },
                    '表格': {
                        develop: {
                            '主路径': iCfg.Svn.DesignerSvn.replace(/\/Develop\/?$/, ''),
                            tags: iCfg.Svn.DesignerSvn.replace(/trunk\/Develop\/?$/, 'tags/tag_')
                        },
                        publish: {
                            '主路径': iCfg.Svn.DesignerSvn.replace(/trunk\/Develop\/?$/, 'branches/publish'),
                            tags: iCfg.Svn.DesignerSvn.replace(/trunk\/Develop\/?$/, 'tags/publish_back_')
                        },
                        publishfix: {
                            '主路径': iCfg.Svn.DesignerSvn.replace(/trunk\/Develop\/?$/, 'branches/publishfix'),
                            tags: iCfg.Svn.DesignerSvn.replace(/trunk\/Develop\/?$/, 'tags/publishfix_back_')
                        }
                    }
                };
            }
        }
        const codeCfg = projCfg['代码']['前台'];
        const userCfg = cfg['svn账户']['拉分支'];
        const time = moment().format('YYYY_MM_DD_HH_mm_ss');
        params.svn = {
            userInfo: ` --username ${userCfg['用户名']} --password ${userCfg['密码']}`,
            urlMap: {
                code: {
                    develop: codeCfg['develop']?.['主路径'],
                    developTag: codeCfg['develop']?.['tags'] + time,
                    publish: codeCfg['publish']?.['主路径'],
                    publishTag: codeCfg['publish']?.['tags'] + time,
                    publishfix: codeCfg['publishfix']?.['主路径'],
                    publishfixTag: codeCfg['publishfix']?.['tags'] + time,
                },
                xls: {
                    develop: projCfg['表格']['develop']?.['主路径'],
                    developTag: projCfg['表格']['develop']?.['tags'] + time,
                    publish: projCfg['表格']['publish']?.['主路径'],
                    publishTag: projCfg['表格']['publish']?.['tags'] + time,
                    publishfix: projCfg['表格']['publishfix']?.['主路径'],
                    publishfixTag: projCfg['表格']['publishfix']?.['tags'] + time,
                }
            }
        };
    }
}
//# sourceMappingURL=SvnCfgParser.js.map