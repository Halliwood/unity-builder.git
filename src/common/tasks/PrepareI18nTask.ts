import { toolchain } from "../../toolchain.js";
import { BaseTask, TaskResult } from "./BastTask.js";

export class PrepareI18nTask extends BaseTask {
    async run(): Promise<TaskResult<void>> {
        // 如果项目需要翻译，则需要更新脚本和后台表格
        if (toolchain.params.channelCfg.localize) {
            await toolchain.i18n.prepareI18n();
        }
        return { success: true, errorCode: 0 };
    }
}
