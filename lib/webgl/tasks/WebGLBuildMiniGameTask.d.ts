import { TaskResult } from "../../common/tasks/BastTask.js";
import { BuildUnityResult, WebGLBuildUnityBaseTask } from "./WebGLBuildUnityBaseTask.js";
import { Nullable } from "../../typings.js";
export declare class WebGLBuildMiniGameTask extends WebGLBuildUnityBaseTask {
    run(): Promise<TaskResult<BuildUnityResult>>;
    get dependencies(): Nullable<string[]>;
}
