import { BaseTask, TaskResult } from "./BastTask.js";
export declare class CheckFileNameTask extends BaseTask {
    run(): Promise<TaskResult<void>>;
}
