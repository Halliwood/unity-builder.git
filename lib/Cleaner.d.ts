import { Ancestor } from './Ancestor.js';
export declare class Cleaner extends Ancestor {
    awake(): Promise<boolean>;
    start(): Promise<void>;
    deletePackages(dir: string): Promise<void>;
    cleanWebGL(dir: string): Promise<void>;
}
