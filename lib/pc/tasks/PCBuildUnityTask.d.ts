import { BaseTask, TaskResult } from "../../common/tasks/BastTask.js";
import { Nullable } from '../../typings';
export declare class PCBuildUnityTask extends BaseTask {
    run(): Promise<TaskResult<void>>;
    private _buildPC;
    /**
     * 修改exe的版本信息
     * @param exeSource
     * @param newVer
     */
    private modifyExeVersionInfo;
    private ensureSignature;
    /**
     * apkinfo.json 被用于检测是否有新包可以下载
     * @param fullVersion
     * @param size
     * @param uploadDir
     * @param apkName
     * @param platName
     * @param jsonName
     */
    protected updateApkVerInfoCfg(fullVersion: string, size: number, uploadDir: string, apkName: string, platName: string, jsonName?: string): void;
    get dependencies(): Nullable<string[]>;
    get skip(): boolean;
}
