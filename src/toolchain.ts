import { SVNClient } from "@taiyosen/easy-svn";
import _ from "lodash";
import { BuildParams } from "./BuildParams.js";
import { HostInfoParser } from "./cfg/HostInfoParser.js";
import { PlatformCfgParser } from "./cfg/PlatformCfgParser.js";
import { SvnCfgParser } from "./cfg/SvnCfgParser.js";
import { CmdOption } from "./CmdOption.js";
import { I18N } from "./common/I18N.js";
import { HtmlMaker } from "./HtmlMaker.js";
import { LogParser } from "./LogParser.js";
import { TaskHelper } from "./TaskHelper.js";
import { FilterPuertsReserved } from "./tools/FilterPuertsReserved.js";
import { JsonBuilder } from "./tools/JsonBuilder.js";
import { ILockFile } from "./tools/lock.js";
import { UnityHelper } from "./tools/UnityHelper.js";
import { ProjIniCfgParser } from "./cfg/ProjIniCfgParser.js";

export declare interface IBuildInfo {
    /**本次构建的资源版本号，请确保在执行BuildAssetsTask后方可访问本字段 */
    resVersion?: number
    /**本次构建的json版本号，请确保在执行JsonBundleTask后方可访问本字段 */
    jsonVersion?: string
}

const __cartoonNames = ['小当家', '樱木花道', '木之本樱', '小可', '宇智波佐助', '哆啦A梦', '大雄', '项少羽', '天明', '月儿', '石兰', '水冰月', '塞巴斯蒂安', '亚伦沃克', '皮卡丘', '鸣人', '爱德华', '旗木卡卡西', '喜洋洋', '灰太狼', '卡卡西', '阿冈', '黑崎一护', '路飞', '索垄山治', '恋次', '越前龙马', '夏尔凡多姆海恩', '柯南', '阿斯兰', '爱德华', '宇智波佐助', '杀生丸', '基拉', '飞鸟真', '罗依', '宇智波佐助', '鲁路修·兰佩路基', '蒙奇·D·路飞', '山治', '江户川柯南', '坂田银时', '通天晓', '濑津美', '中务椿', '南燕', '中原须奈子', '爱迪', '高桥美哄', '薇儿', 'pm梦妖', '加藤故', '由贵瑛里', '唯世', '稻尾一久', '猫熊达', '小次郎', '东宫闲雅', '蔷薇圣母', '高杉', '帕伽索斯', '白龙', '三月呀', '杰斯迪比', '萨姆伊', '提尔利亚', '真宫樱', '秦时明月雪女', '李娜丽', '宫老', '福福鼠', '库洛姆·髑骸', '斯梅纳基', '恐山安娜', '三浦春', '银河团', '流川枫', '露芝亚', '巴利安', '花月', '一辉', '黑崎─护', '李小狼', '奇牙', '大空翼', '卡嘉莉', '波雅·汉库克', '奈落', '橘一夏', '贝露丹迪', '多啦A梦', '灰原哀', '亚由美', '奥路菲', '藤原佐为', '我爱罗', '蜡笔小新'];

const __dirname = '';
const startAt = _.now();
const startByTimer= false;
const startUser = __cartoonNames[_.random(0, __cartoonNames.length - 1, false)];
const lockfile: ILockFile | null = null as ILockFile | null;
const buildMessages: string[] = [];
const appendLogURL = false;
const hostInfoParser = new HostInfoParser();
const svnCfgParser = new SvnCfgParser();
const platformCfgParser = new PlatformCfgParser();
const projIniCfgParser = new ProjIniCfgParser ();
const option: CmdOption = {} as CmdOption;
const params: BuildParams = {} as BuildParams;
const svnClient = new SVNClient();
const buildInfo: IBuildInfo = {
    resVersion: 0,
    jsonVersion: ''
}
const i18n = new I18N();
const unity = new UnityHelper();
const logParser = new LogParser();
const jsonBuilder = new JsonBuilder();
const htmlMaker = new HtmlMaker();
const taskHelper = new TaskHelper();
const filterPuertsReserved = new FilterPuertsReserved();
const wechatDevtools = '';

export const toolchain = { __dirname, startAt, startByTimer, startUser, lockfile, buildMessages, appendLogURL, hostInfoParser, svnCfgParser, platformCfgParser, projIniCfgParser, option, params, svnClient, buildInfo, i18n, unity, logParser, jsonBuilder, htmlMaker, taskHelper, filterPuertsReserved, wechatDevtools };
