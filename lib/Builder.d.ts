import { Ancestor, IAdditionalMsg } from './Ancestor.js';
export declare abstract class Builder extends Ancestor {
    awake(): Promise<boolean>;
    start(): Promise<void>;
    private setBuildStateFile;
    private saveBuildInfo;
    getBuildConclusion(): Promise<string>;
    getAdditionalMessages(): Promise<IAdditionalMsg[]>;
}
