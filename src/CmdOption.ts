import { PlatformTypes, TLayaPlatformTypes, WebglRunTime } from "./typings.js";

export declare type EngineType = 'unity' | 'laya' | 'flash';
export declare type BuildTask = 'Build' | 'PullPublish' | 'makeI18nXlsx' | 'Clean' | 'hudsonJob' | 'SetTiShen' | 'UpdateTiShenIOS' | 'SwitchPublishfix' | 'DetectProxy';
export declare type TCheckI18nRule = 'FormatMissing' | 'FormatError' | 'TermEN' | 'TermCN';
export declare type TBuiltInTaskTpl = 'OnlyCode' | 'OnlyJson';

interface UnityCmdOption {
    platform: PlatformTypes;
    engine?: 'unity';
}
interface LayaCmdOption {
    platform: TLayaPlatformTypes;
    engine?: 'laya';
}
interface BaseCmdOption {    
    projectName: string;
    projectRoot?: string;
    gameIds: string;
    oldToolsRoot: string;
    buildExe: boolean;
    webglCompressTex?: boolean;
    webglRuntime: WebglRunTime;
    uploadRes: boolean;
    setMinApp?: boolean;
    uploadMinigameDesc: string;
    minigameEnableDebugLog: boolean;
    minigameShowTimeLogModal: boolean;
    minigameEnableProfileStats: boolean;
    task?: BuildTask;
    taskParameter?: string;
    shortOutput?: boolean;
    skip?: string;
    execute?: string;
    /**使用内置的执行任务组合模板 */
    executeTpl?: TBuiltInTaskTpl;
    debug?: boolean;
    checkI18n?: string;
    isTiShen?: boolean;
    report?: string;
}
export declare type CmdOption = BaseCmdOption & (UnityCmdOption | LayaCmdOption);
