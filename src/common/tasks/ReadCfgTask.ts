import { toolchain } from "../../toolchain.js";
import { BaseTask, TaskResult } from "./BastTask.js";

export class ReadCfgTask extends BaseTask {
    async run(): Promise<TaskResult<void>> {
        if (this.cmdOption.engine == "laya") {
        } else {
            await toolchain.unity.getUnityInfo(toolchain.params);
        }
        await toolchain.platformCfgParser.getChannelCfg(this.cmdOption, toolchain.params);
        const channelCfg = toolchain.params.channelCfg;
        console.log('channelCfg:', channelCfg);

        // 根据unity版本确定安卓环境，如ndk
        await toolchain.unity.decideAndroidEnv();

        // 检查配置
        if (this.cmdOption.engine == 'unity') {
            if (this.cmdOption.platform == 'iOS') {
                if (!channelCfg.bundleVersion) {
                    throw new Error('bundleVersion not set in cfg.json!');
                }
                if (!channelCfg['ios-sdk-params']) {
                    throw new Error('ios-sdk-params not set in cfg.json!');
                }    
            } else if (this.cmdOption.platform == 'WebGL' && this.cmdOption.webglRuntime == 'minigame') {
                if (!channelCfg.wx) {
                    throw new Error('wx not set in cfg.json!');
                }
            } else if (this.cmdOption.platform == 'WebGL' && this.cmdOption.webglRuntime == 'douyin') {
                if (!channelCfg.douyin) {
                    throw new Error('douyin not set in cfg.json!');
                }
            }
        }

        return { success: true, errorCode: 0 };
    }

    get executeAlways(): boolean {
        return true;
    }
}
