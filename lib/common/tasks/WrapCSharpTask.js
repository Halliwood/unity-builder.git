import { BaseTask } from "./BastTask.js";
import { toolchain } from "../../toolchain.js";
export class WrapCSharpTask extends BaseTask {
    async run() {
        // 混合多个平台的必须wrap
        const sps = toolchain.params.projGlobalCfg?.support_platforms?.toLowerCase().split(/,\s*/);
        if (sps != null && sps.includes('webgl') && (sps.includes('pc') || sps.includes('android') || sps.includes('ios')) && this.cmdOption.platform.toLowerCase() != sps[0]) {
            // 第一个平台视为主平台，即不需要重新wrap
            await toolchain.unity.runUnityCommand(this.cmdOption, ['-batchmode', '-projectPath', toolchain.params.workSpacePath, '-executeMethod', 'Puerts.Editor.Generator.UnityMenu.GenerateCode', '-quit'], 'Wrap C#', true);
        }
        else {
            console.log('skip wrap csharp');
        }
        return { success: true, errorCode: 0 };
    }
    get skip() {
        // 如果不重新打包则不需要重新wrap
        return !this.cmdOption.buildExe;
    }
}
//# sourceMappingURL=WrapCSharpTask.js.map