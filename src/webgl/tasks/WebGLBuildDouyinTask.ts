import fs from "fs-extra";
import path from "path";
import { TaskResult } from "../../common/tasks/BastTask.js";
import { toolchain } from "../../toolchain.js";
import { WebGLEnv } from "../WebGLEnv.js";
import { BuildUnityResult, WebGLBuildUnityBaseTask } from './WebGLBuildUnityBaseTask.js';
import { replaceInFileOrThrow } from "../../tools/vendor.js";

export class WebGLBuildDouyinTask extends WebGLBuildUnityBaseTask {
    async run(): Promise<TaskResult<BuildUnityResult>> {
        // 修改appid
        const cfgFile = path.join(toolchain.params.workSpacePath, 'Assets/Editor/StarkBuilderSetting.asset');
        replaceInFileOrThrow(cfgFile, /(?<=_appId: )\S+/, toolchain.params.channelCfg.douyin!.appid);

        // 调用抖音sdk命令
        const result = await this.callUnity(WebGLEnv.ExportDouyinRoot, 'BuildForDouyin');
        if (!result.success) return result;

        // 写入appVer.txt记录application版本，以供手动上传时使用
        const appVerFile = path.join(toolchain.params.workSpacePath, WebGLEnv.DouyinAppVerFile);
        await fs.writeFile(appVerFile, result.data!.version, 'utf-8');

        return result;
    }
}
