# unity-builder

#### 介绍

风月专用，unity项目构建工具。

#### 安装教程

`npm i unity-builder -g`

#### 使用说明

* -n, --project-name 项目名-[develop|publish]
* -i, --game-ids 游戏ID
* -t, --old-tools-root 旧版工具目录（为了复用其中的一些项目配置文件）
* -e, --build-exe 构建新的exe/apk/ipa
* -p, --platform Android|Ios|Windows

```
unity-builder -n "%ProjectName%-develop" -i %Gameids% -t '%ScriptPath%' -e -p Windows
```