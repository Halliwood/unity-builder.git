import { Builder } from "../Builder.js";
import { CmdOption } from "../CmdOption.js";
import { IAdditionalMsg } from "../Ancestor.js";
/**
 * WebGL小游戏构建器
 */
export declare class WebGLMiniGameBuilder extends Builder {
    protected cmdOption: CmdOption;
    constructor(cmdOption: CmdOption);
    getAdditionalMessages(): Promise<IAdditionalMsg[]>;
}
