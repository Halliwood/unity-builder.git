export declare class Chaos {
    private leadLetters;
    private allLetters;
    private size;
    private pool;
    private cache;
    init(size: number, cache: Record<string, string>): void;
    getChaos(s: string): string;
}
