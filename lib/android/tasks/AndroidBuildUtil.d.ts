import { BuildParams } from '../../BuildParams.js';
import { CmdOption } from '../../CmdOption.js';
/**
 * 将xsdkLibrary复制到gradle工程路径下
 * 替换宏定义
 */
export declare class CopyXsdkLibrary {
    private xsdkPath;
    private params;
    private exportproj;
    private cmdOption;
    constructor(xsdkPath: string, params: BuildParams, exportproj: string, cmdOption: CmdOption);
    copy(): Promise<boolean>;
}
/**
 * 将xsdkLibrary嵌入到gradle工程中
 */
export declare class ModifyGradle {
    private exportproj;
    private changeXSdkPackage;
    private bundleId;
    private appModuleName;
    constructor(exportproj: string, changeXSdkPackage: boolean, bundleId: string, appModuleName: "launcher" | "app");
    private mergeGradleProperties;
    private modifyGradle;
    private modifyManifest;
    insert(): Promise<{
        [key: string]: any;
    }>;
}
/**
 * 构建gradle工程，生成apk/aab
 */
export declare class GradleBuild {
    private exportproj;
    private xsdkPath;
    private gradleProperties;
    private resLibrary;
    constructor(exportproj: string, xsdkPath: string, gradleProperties: {
        [key: string]: string;
    }, resLibrary: 'unityLibrary' | 'app');
    build(isAAB: boolean): Promise<void>;
}
/**
 *  将构建好的apk/aab放到 hudson下 供内网下载
 */
export declare class UploadApk {
    private params;
    private exportproj;
    private version;
    private appModuleName;
    constructor(params: BuildParams, exportproj: string, version: string, appModuleName: "launcher" | "app");
    upload(): Promise<string>;
    private copyApkTo;
}
export declare let recordApk: (apkName: string) => Promise<void>;
