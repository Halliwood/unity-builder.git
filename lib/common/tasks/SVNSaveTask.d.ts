import { BaseTask, TaskResult } from "./BastTask.js";
export declare interface SVNInfo {
    json: number;
}
/**
 * 保存本次SVN版本号信息。
 */
export declare class SVNSaveTask extends BaseTask {
    run(): Promise<TaskResult<void>>;
}
