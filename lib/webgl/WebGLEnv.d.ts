export declare class WebGLEnv {
    /**支持贴图压缩的unity最高版本 */
    static readonly CompressTexMaxVer = "2022.3.14";
    static readonly MD5Len = 6;
    static readonly MangleBak = "../mangle_bak";
    static readonly MangleReserved = "../mangle/manglereserved.txt";
    static readonly MangleCache = "../mangle/manglecached.txt";
    static readonly PuertsWebgl = "Assets/Puerts_webgl";
    static readonly PuertsWebglJSbuild = "Assets/Puerts_webgl_jsbuild";
    static readonly MiniGameConfigAsset = "Assets/WX-WASM-SDK/Editor/MiniGameConfig.asset";
    static readonly MiniGameConfigAssetV2 = "Assets/WX-WASM-SDK-V2/Editor/MiniGameConfig.asset";
    static readonly ExportbrowserRoot = "../export_browser";
    static readonly ExportBuildRoot = "Build";
    static readonly SvrBrowserRoot = "browser";
    static readonly ExportMiniGameRoot = "../export_minigame";
    static readonly MiniGameJsResources = "puerts_minigame_js_resources";
    static readonly MiniGameRoot = "minigame";
    static readonly MiniGameJs = "game.js";
    static readonly WebglRoot = "webgl";
    static readonly WebglCompressedOut = "webgl-min";
    static readonly MiniGameAssetRoot = "assets/wx";
    static readonly indexHtml = "index.html";
    static readonly PuertsRuntimeJs = "puerts-runtime.js";
    static readonly PuertsBrowserJsResourcesJs = "puerts_browser_js_resources.js";
    static readonly MiniGameAppVerFile: string;
    static readonly MiniGameResVerFile: string;
    static readonly MiniGameQrcode: string;
    static readonly MiniGamePreviewOutput: string;
    static readonly MiniGameUploadOutput: string;
    static readonly ExportDouyinRoot = "../export_douyin";
    static readonly DouyinJsRoot = "Assets/StreamingAssets/__cp_js_files";
    static readonly DouyinAppVerFile: string;
}
