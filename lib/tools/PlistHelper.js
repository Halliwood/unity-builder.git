import fs from "fs-extra";
import plist from "plist";
export var ECertType;
(function (ECertType) {
    ECertType["Invalid"] = "";
    ECertType["Dev"] = "development";
    ECertType["AdHoc"] = "ad-hoc";
    ECertType["Store"] = "app-store";
})(ECertType || (ECertType = {}));
export class PlistHelper {
    static async read(file) {
        const content = await fs.readFile(file, 'utf-8');
        return plist.parse(content);
    }
    static async writeProvision(file, provisionName, bundleID, certType) {
        const obj = {
            compileBitcode: false,
            uploadSymbols: false,
            method: certType,
            provisioningProfiles: {
                buildleID: bundleID,
                bundleID: provisionName
            }
        };
        await PlistHelper.write(file, obj);
    }
    static async write(file, obj) {
        const content = plist.build(obj);
        await fs.writeFile(file, content, 'utf-8');
    }
}
//# sourceMappingURL=PlistHelper.js.map