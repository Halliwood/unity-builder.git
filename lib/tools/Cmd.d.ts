import { SpawnOptions } from "child_process";
interface CmdOption extends SpawnOptions {
    silent?: boolean;
    logPrefix?: string;
    outputFormatter?: (raw: string) => string;
    /**部分命令并不会自动退出，当接收到预期输出后即返回 */
    expect?: string;
}
export interface ExecuteError {
    code: number;
    error?: Error;
}
export declare class Cmd {
    private _output;
    private _errorOutput;
    private resolved;
    runNodeModule(moduleName: string, params?: string[], options?: CmdOption): Promise<number>;
    run(command: string, params?: string[], options?: CmdOption): Promise<number>;
    get output(): string;
    get errorOutput(): string;
}
export {};
