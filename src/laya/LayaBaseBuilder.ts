import _ from 'lodash';
import path from 'path';
import { Ancestor } from '../Ancestor.js';
import { SVNOperationTask } from '../common/tasks/SVNOperationTask.js';
import { toolchain } from '../toolchain.js';
import { checkDiskAvailable, gb2byte } from '../tools/vendor.js';
import { layatoolchain } from '../layatoolchain.js';
import { LayaEnv } from './LayaEnv.js';
import { LayaPreBuildTask } from './tasks/LayaPreBuildTask.js';
import { BuildLock, checkOrLockWorkspace } from '../tools/lock.js';

export abstract class LayaBaseBuilder extends Ancestor {

    async awake(): Promise<boolean> {
        await this.getParams();
        return true;
    }

    async start(): Promise<void> {
        // 打印参数
        // console.log('构建参数', toolchain.params);

        // 检查构建锁
        await checkOrLockWorkspace(BuildLock);

        // 检查磁盘
        await checkDiskAvailable(toolchain.params.workSpacePath, gb2byte(5));

        // 组装toolchain
        layatoolchain.vars.publishRoot = path.join(toolchain.params.workSpacePath, LayaEnv.ReleaseRoot, this.cmdOption.platform);
        
        // 统一添加公共前置任务和后置任务
        this.series.unshift(
            new SVNOperationTask(this.cmdOption),
            new LayaPreBuildTask(this.cmdOption),
        );

        // 开始执行所有任务
        await this.startSeries();
    }
}
