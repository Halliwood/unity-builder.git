export class BaseTask {
    cmdOption;
    constructor(cmdOption) {
        this.cmdOption = cmdOption;
    }
    /**是否永远执行，如为true则无视skip命令 */
    get executeAlways() {
        return false;
    }
    /**是否可跳过，注意只允许使用静态条件判断，不能使用项目配置等进行判断 */
    get skip() {
        return false;
    }
    /**依赖前置任务，被依赖的任务无视skip命令 */
    get dependencies() {
        return null;
    }
    get taskName() {
        return this.constructor.name;
    }
}
//# sourceMappingURL=BastTask.js.map