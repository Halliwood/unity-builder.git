import { BaseTask } from "../../common/tasks/BastTask.js";
import { Nullable } from "../../typings.js";
export declare abstract class LayaPostPublishBaseTask extends BaseTask<void> {
    getVersionCode(): Promise<string>;
    get dependencies(): Nullable<string[]>;
}
