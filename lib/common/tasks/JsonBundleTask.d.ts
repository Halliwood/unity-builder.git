import { BaseTask, TaskResult } from "./BastTask.js";
/**json不打ab包，直接上传到cdn，需在cfg.json中配置开启，目前仅在webgl模型下使用 */
export declare class JsonBundleTask extends BaseTask<void> {
    run(): Promise<TaskResult<void>>;
}
