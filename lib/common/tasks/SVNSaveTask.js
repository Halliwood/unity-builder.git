import fs from 'fs-extra';
import path from 'path';
import { toolchain } from "../../toolchain.js";
import { BaseTask } from "./BastTask.js";
/**
 * 保存本次SVN版本号信息。
 */
export class SVNSaveTask extends BaseTask {
    async run() {
        if (toolchain.option.engine != 'laya') {
            // 目前只支持unity项目
            const jsonRoot = path.join(toolchain.params.workSpacePath, 'Assets/AssetSources/data');
            const jsonRevision = await toolchain.svnClient.getRevision(jsonRoot);
            console.log('json revision:', jsonRevision);
            const svnInfoFile = path.join(toolchain.params.workSpacePath, `../.build/svnInfo.json`);
            await fs.ensureFile(svnInfoFile);
            const info = { json: jsonRevision };
            await fs.writeJSON(svnInfoFile, info, { spaces: 2 });
        }
        return { success: true, errorCode: 0 };
    }
}
//# sourceMappingURL=SVNSaveTask.js.map