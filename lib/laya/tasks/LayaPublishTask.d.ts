import { BaseTask, TaskResult } from "../../common/tasks/BastTask.js";
/**Laya导出工程 */
export declare class LayaPublishTask extends BaseTask<void> {
    run(): Promise<TaskResult<void>>;
}
