import { LayaBaseBuilder } from "./LayaBaseBuilder.js";
import { LayaBuildPatchTask } from "./tasks/LayaBuildPatchTask.js";
import { LayaCompileTask } from "./tasks/LayaCompileTask.js";
import { LayaPostPublishWxgameTask } from "./tasks/LayaPostPublishWxgameTask.js";
import { LayaPublishTask } from "./tasks/LayaPublishTask.js";
import { LayaUploadResTask } from "./tasks/LayaUploadResTask.js";
import { LayaVersionedTask } from "./tasks/LayaVersionedTask.js";
export class LayaWxgameBuilder extends LayaBaseBuilder {
    cmdOption;
    constructor(cmdOption) {
        super(cmdOption);
        this.cmdOption = cmdOption;
        this.series.push(new LayaCompileTask(this.cmdOption), new LayaPublishTask(this.cmdOption), new LayaVersionedTask(this.cmdOption), new LayaBuildPatchTask(this.cmdOption), new LayaPostPublishWxgameTask(this.cmdOption), new LayaUploadResTask(this.cmdOption));
    }
}
//# sourceMappingURL=LayaWxgameBuilder.js.map