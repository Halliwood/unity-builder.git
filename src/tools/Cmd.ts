import os from 'os';
import { spawn, SpawnOptions } from "child_process";
import moment from 'moment';
import _ from 'lodash';

interface CmdOption extends SpawnOptions {
    silent?: boolean;
    logPrefix?: string;
    outputFormatter?: (raw: string) => string;
    /**部分命令并不会自动退出，当接收到预期输出后即返回 */
    expect?: string;
}

export interface ExecuteError {
    code: number,
    error?: Error
}

export class Cmd {
    private _output: string = '';
    private _errorOutput: string = '';
    private resolved = false;

    runNodeModule(moduleName: string, params?: string[], options?: CmdOption): Promise<number> {
        if(os.type() == 'Windows_NT' && !moduleName.match(/\.cmd$/)) {
            moduleName += '.cmd';
        }
        return this.run(moduleName, params, options);
    }

    run(command: string, params?: string[], options?: CmdOption): Promise<number> {
        this._output = '';
        this._errorOutput = '';
        this.resolved = false;
        // options = Object.assign(options || {}, { cwd: this.cfg.cwd });
        const startAt = _.now();
        return new Promise((resolve: (data: number) => void, reject: (error: ExecuteError) => void) => {
            if(!options?.silent) console.log(`run command: ${command}, params:`, params, options);
    
            if(!options) {
                options = {};
            }
            if(!params) params = [];
            options.stdio = 'pipe';
            let proc = spawn(command, params, options);
    
            let lastData: string | undefined = undefined;
            let repeatCnt = 0;
            proc.stdout!.on('data', (data) => {
                let dataStr = String(data);
                if (options?.outputFormatter) dataStr = options.outputFormatter(dataStr);
                if (dataStr) {
                    if (dataStr !== lastData) {
                        this._output += dataStr;
                        if(options?.logPrefix) {
                            dataStr = options.logPrefix + dataStr;
                        }
                        let print = '\n';
                        if (repeatCnt > 1) {
                            print = `  repeat #${repeatCnt}...\n`;
                        }
                        print += moment().format('HH:mm:ss:SSS ') + dataStr;
                        if(!options?.silent) process.stdout.write(print);
                        lastData = dataStr;
                        repeatCnt = 0;
                    } else {
                        repeatCnt++;
                    }
                    if (options?.expect && dataStr.includes(options?.expect)) {
                        resolve(0);
                    }
                }
            });
    
            proc.stderr!.on('data', (data) => {
                // 不一定代表进程exitcode != 0，可能只是进程调用了console.error
                let dataStr = String(data);
                if (options?.outputFormatter) dataStr = options.outputFormatter(dataStr);
                if (dataStr) {
                    if (dataStr !== lastData) {
                        this._output += dataStr;
                        this._errorOutput += dataStr;
                        if(options?.logPrefix) {
                            dataStr = options.logPrefix + dataStr;
                        }
                        let print = '\n';
                        if (repeatCnt > 1) {
                            print = `  repeat #${repeatCnt}...\n`;
                        }
                        print += moment().format('HH:mm:ss:SSS ') + dataStr;
                        if(!options?.silent) process.stderr.write(print);
                        lastData = dataStr;
                        repeatCnt = 0;
                    } else {
                        repeatCnt++;
                    }
                }
                if (options?.expect && dataStr.includes(options?.expect)) {
                    resolve(0);
                }
            });
    
            // 进程错误
            proc.on('error', (error: Error) => {
                if(!options?.silent) console.error(error);
                reject({ code: 1, error });
            });
    
            // 进程关闭
            proc.on('close', (code: number) => {
                const now = _.now();
                const timeslapse = now - startAt;
                let timeStr = moment(timeslapse).utcOffset(0).format(moment.HTML5_FMT.TIME_MS);
                if(!options?.silent) console.log(`process closed with exit code: ${code}, cost time: ${timeStr}`);
                if (!this.resolved) {
                    this.resolved = true;
                    if(code == 0) {
                        resolve(0);
                    } else {
                        let errMsg = `process closed with exit code: ${code}`;
                        if(options?.logPrefix) {
                            errMsg = options.logPrefix + errMsg;
                        }
                        reject({ code, error: new Error(errMsg) });
                    }
                } else {
                    console.log('already resolved, skip');
                }
            });

            proc.on('exit', (code: number) => {
                if(!options?.silent) console.log(`process exits with exit code: ${code}`);
                // 经测试，unity如调用EditorApplication.Exit，不会收到close，此处做个处理，收到exit后10秒内没有close则直接resolve
                setTimeout(() => {
                    if (!this.resolved) {
                        console.log('no close event fired, force resolve');
                        this.resolved = true;
                        if(code == 0) {
                            resolve(0);
                        } else {
                            let errMsg = `process exits with exit code: ${code}`;
                            if(options?.logPrefix) {
                                errMsg = options.logPrefix + errMsg;
                            }
                            reject({ code, error: new Error(errMsg) });
                        }
                    }
                }, 10000);
            });
        });
    }

    public get output(): string {
        return this._output;
    }

    public get errorOutput(): string {
        return this._errorOutput;
    }
}