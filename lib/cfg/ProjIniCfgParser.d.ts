interface ProjectCfg {
    Project: {
        code: string;
        Name: string;
        Alias?: string;
        ProjectType?: 'H5ts' | 'Unity' | 'Laya';
        chatid?: string;
    };
    Svn: {
        ClientSvn: string;
        DesignerSvn: string;
        SvrSvn: string;
    };
    Build?: {
        ProjRoot: string;
    };
}
export declare class ProjIniCfgParser {
    /**
     * 通过项目中文名获取其ini配置。
     * @param projectName
     * @returns
     */
    getProjIniCfg(projectName: string): Promise<ProjectCfg | null>;
}
export {};
