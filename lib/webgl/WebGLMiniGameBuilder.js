import { Builder } from "../Builder.js";
import { WebGLInstallTask } from "./tasks/WebGLInstallTask.js";
import { WebGLBuildMiniGameTask } from "./tasks/WebGLBuildMiniGameTask.js";
import { WebGLPostBuildMiniGameTask } from "./tasks/WebGLPostBuildMiniGameTask.js";
import { BuildAssetsTask } from "../common/tasks/BuildAssetsTask.js";
import { PreBuildAppTask } from "../common/tasks/PreBuildAppTask.js";
import { WebGLCompressTextureTask } from "./tasks/WebGLCompressTextureTask.js";
import { WebGLCheckCfgTask } from "./tasks/WebGLCheckCfgTask.js";
import { toolchain } from "../toolchain.js";
import { WebGLMiniGameUploadTask } from "./tasks/WebGLMiniGameUploadTask.js";
import { I18NTask } from "../common/tasks/I18NTask.js";
import { TsObfuscateTask } from "../common/tasks/TsObfuscateTask.js";
import { JsonBundleTask } from "../common/tasks/JsonBundleTask.js";
import { WebGLMiniGamePreloadTask } from "./tasks/WebGLMiniGamePreloadTask.js";
import { UnityTscTask } from "../common/tasks/UnityTscTask.js";
/**
 * WebGL小游戏构建器
 */
export class WebGLMiniGameBuilder extends Builder {
    cmdOption;
    constructor(cmdOption) {
        super(cmdOption);
        this.cmdOption = cmdOption;
        this.series.push(new WebGLCheckCfgTask(this.cmdOption), new WebGLInstallTask(this.cmdOption), new I18NTask(this.cmdOption), new TsObfuscateTask(this.cmdOption), new UnityTscTask(this.cmdOption), new BuildAssetsTask(this.cmdOption), new JsonBundleTask(this.cmdOption), new PreBuildAppTask(this.cmdOption), new WebGLBuildMiniGameTask(this.cmdOption), new WebGLCompressTextureTask(this.cmdOption), new WebGLMiniGamePreloadTask(this.cmdOption), new WebGLPostBuildMiniGameTask(this.cmdOption), new WebGLMiniGameUploadTask(this.cmdOption));
    }
    async getAdditionalMessages() {
        const msgs = await super.getAdditionalMessages();
        const compressTexRst = toolchain.taskHelper.getResult(WebGLCompressTextureTask.name);
        if (compressTexRst != null) {
            if (compressTexRst.data?.guidence) {
                msgs.push({
                    type: 'markdown',
                    msg: compressTexRst.data?.guidence
                });
            }
        }
        const uploadRst = toolchain.taskHelper.getResult(WebGLMiniGameUploadTask.name);
        if (uploadRst != null) {
            if (uploadRst.data?.success) {
                if (uploadRst.data.result?.error) {
                    msgs.push({
                        type: 'markdown',
                        msg: `## 注意
\`小游戏\`上传成功，但检测到如下问题：
${uploadRst.data.result.error}`
                    });
                }
                if (toolchain.params.projectType == 'publish') {
                    // 发送提审提示
                    msgs.push({
                        type: 'markdown',
                        msg: `## 注意
    \`小游戏\`如需更新**CS代码**和**ts代码**均需**提审**，提审步骤如下：
    1. 前往[设置提审](http://builder.fygame.com:8080/hudson/job/${toolchain.params.channelCfg.productName}-设置提审/)
    2. 前往[发布更新](http://builder.fygame.com:3000/#/pages/tools/publish)，注意判断是否只更新\`提审服\`
    3. 前往[微信公众平台](https://mp.weixin.qq.com/)提交审核

    审核通过后，在MP后台对外发布。`
                    });
                    const ver = await toolchain.unity.getFullBuildVer();
                    msgs.push({
                        type: 'markdown',
                        msg: `\`小游戏\`提审步骤一：如果需要提审本次版本，请前往[设置提审](http://builder.fygame.com:8080/hudson/job/${toolchain.params.channelCfg.productName}-设置提审/)，版本号请填写\`${ver}\``
                    });
                }
            }
            else {
                msgs.push({
                    type: 'markdown',
                    msg: `## 注意
\`小游戏\`上传失败，请通过以下任意途径进行上传：
1. 前往[小游戏上传](http://builder.fygame.com:8080/hudson/job/${toolchain.params.channelCfg.productName}-小游戏上传/)
2. 使用向日葵远程连接构建机，打开微信开发者工具手动上传`
                });
            }
        }
        return msgs;
    }
}
//# sourceMappingURL=WebGLMiniGameBuilder.js.map