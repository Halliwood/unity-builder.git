export declare class CommonEnv {
    static readonly PublishAssets = "publish/assets";
    static readonly EditorBuildSettingsAsset = "ProjectSettings/EditorBuildSettings.asset";
    static readonly TsSrc = "TsScripts";
    static readonly TsOutput = "TsScripts/.dist";
    static readonly PuertsWebGLJSLib = "Assets/Puerts_webgl/Runtime/Plugins/WebGL";
    static readonly PuertsDLLMockLibJS = "Assets/Puerts_webgl/Javascripts~/PuertsDLLMock";
    static readonly PuertsJSLib = "Editor/Resources/puerts";
    static readonly PuerGen = "Assets/Gen";
    static readonly PuertsWebglRoot = "Assets/Puerts_webgl";
    static readonly I18N_TS = "TsScripts/System/i18n/i18n.ts";
    static readonly I18N_CS = "Assets/Scripts/i18n/i18n.cs";
    static readonly RESLOADER_CS = "Assets/Scripts/game/AssetLoader/ResLoader.cs";
    static readonly Root_Unity = "Assets/Scenes/root.unity";
    static readonly Root_webgl_Unity = "Assets/Scenes/root_webgl.unity";
    static readonly ComLib = "libs/cjs/com";
    static readonly VersionCodeTxt = "build/versionCode.txt";
}
