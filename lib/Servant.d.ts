import { Ancestor, IAdditionalMsg } from "./Ancestor.js";
export declare class Servant extends Ancestor {
    private conclusion;
    private additionalMessages;
    awake(): Promise<boolean>;
    start(): Promise<void>;
    private makeI18nXlsx;
    private pullPublish;
    private setTiShen;
    private UpdateTiShenIOS;
    private switchPublishfix;
    private detectProxy;
    getBuildConclusion(): Promise<string>;
    getAdditionalMessages(): Promise<IAdditionalMsg[]>;
}
