import { BaseTask, TaskResult } from "./BastTask.js";
/**修改Unity图片webgl压缩格式为astc8*8 */
export declare class ModifyUnityTextureTask extends BaseTask<void> {
    run(): Promise<TaskResult<void>>;
}
