import { Builder } from '../Builder.js';
import { CmdOption } from '../CmdOption.js';
import { IAdditionalMsg } from '../Ancestor.js';
export declare class AndroidBuilder extends Builder {
    protected cmdOption: CmdOption;
    constructor(cmdOption: CmdOption);
    getBuildConclusion(): Promise<string>;
    getAdditionalMessages(): Promise<IAdditionalMsg[]>;
}
