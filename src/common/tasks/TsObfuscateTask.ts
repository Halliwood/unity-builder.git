import { toolchain } from "../../toolchain.js";
import { BaseTask, TaskResult } from "./BastTask.js";
import { JsonTerser } from "../../tools/JsonTerser.js";

export class TsObfuscateTask extends BaseTask {

    async run(): Promise<TaskResult<void>> {
        // 混淆json
        if (toolchain.params.channelCfg.terserJson) {
            const jt = new JsonTerser();
            await jt.run(toolchain.params.workSpacePath);
        }
        return { success: true, errorCode: 0 };
    }
}
