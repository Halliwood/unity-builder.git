import fs from "fs-extra";
import ip from "ip";
import path from "path";
import xlsx from 'xlsx';
import { toolchain } from "./toolchain.js";
import { env } from "./env.js";
export class Ancestor {
    cmdOption;
    series = [];
    constructor(cmdOption) {
        this.cmdOption = cmdOption;
    }
    async startSeries() {
        let tpl = null;
        if (toolchain.option.executeTpl) {
            tpl = await this.readBuiltInTpl();
            // 为防止新加了Task忘了维护模板表格，此处检查所有task均需显式配置
            for (const task of this.series) {
                if (!(task.taskName in tpl)) {
                    console.error(`built in template out of date! ${task.taskName} is not defined!`);
                    process.exit(1);
                }
            }
        }
        const skipStatus = {};
        for (const task of this.series) {
            if (task.taskName in skipStatus) {
                console.error(`[Series] Task ${task.taskName} repeated, Please check!`);
                process.exit(1);
            }
            if (!task.executeAlways &&
                (toolchain.params.skipTasks?.includes(task.taskName) ||
                    toolchain.params.executeTasks != null && !toolchain.params.executeTasks.includes(task.taskName) ||
                    tpl != null && tpl[task.taskName] == '×' ||
                    task.skip)) {
                skipStatus[task.taskName] = true;
            }
            else {
                skipStatus[task.taskName] = false;
                if (task.dependencies != null) {
                    for (const dp of task.dependencies) {
                        console.log(`[Series] Task ${dp} cannot be skipped caurse it's depended by ${task.taskName}`);
                        skipStatus[dp] = false;
                    }
                }
            }
        }
        const willRunTasks = [];
        for (const taskName in skipStatus) {
            if (!skipStatus[taskName]) {
                willRunTasks.push(taskName);
            }
        }
        console.log(`[Series] The following task will be runned: ${willRunTasks.join(',')}`);
        for (const task of this.series) {
            if (skipStatus[task.taskName]) {
                console.log(`[${task.taskName}] task skipped ..............................`);
                continue;
            }
            console.log(`[${task.taskName}] task begin ..............................`);
            console.time(`[${task.taskName}]`);
            const result = await task.run();
            toolchain.taskHelper.saveResult(task.taskName, result);
            console.timeEnd(`[${task.taskName}]`);
            if (!result.success) {
                const message = result.message || 'task failed';
                console.error(`[${task.taskName}] ${message}`);
                process.exit(result.errorCode);
            }
        }
    }
    async getParams() {
        const [projectName, projectType] = this._getProjInfo(this.cmdOption.projectName);
        if (this.cmdOption.skip) {
            toolchain.params.skipTasks = this.cmdOption.skip.split(/,\s*/);
        }
        if (this.cmdOption.execute) {
            toolchain.params.executeTasks = this.cmdOption.execute.split(/,\s*/);
        }
        console.log('The following task will be skipped:', toolchain.params.skipTasks ?? '');
        toolchain.params.ip = ip.address();
        toolchain.params.projectName = projectName;
        toolchain.params.projectType = projectType;
        // 发test默认JsonDiff
        const report = this.cmdOption.report || (projectType == 'publish' ? 'JsonDiff' : '');
        toolchain.params.reportLvs = report.split(/,\s*/);
        // if(this.cmdOption.platform == 'iOS') {
        //     toolchain.params.platformPath = 'platformIos';
        // } else {
        //     toolchain.params.platformPath = 'platform';
        // }
        toolchain.params.platformPath = 'platform';
        // 更新ini配置
        if (env.projIniRoot && fs.existsSync(env.projIniRoot)) {
            await toolchain.svnClient.cleanup(false, env.projIniRoot);
            await toolchain.svnClient.update(env.projIniRoot);
        }
        await toolchain.hostInfoParser.getBuildCfg(this.cmdOption, toolchain.params);
        await toolchain.svnCfgParser.getSVNCfg(this.cmdOption, toolchain.params);
    }
    _getProjInfo(projectName) {
        return projectName.split('-');
    }
    async cleanProj(workspacePath, responsitory) {
        console.time('SVN更新耗时');
        console.log('=============================clean project===============================');
        // 有些项目svn可能没有project这一级，比如一些临时拉的测试项目
        // 先检测是否有project
        let svnContainsProject = true;
        try {
            await toolchain.svnClient.info(responsitory + '/project', '--show-item', 'kind');
        }
        catch (e) {
            if (e instanceof Error) {
                if (e.message.includes('svn: E200009: Could not display info for all targets because some targets don\'t exist')) {
                    svnContainsProject = false;
                }
            }
        }
        console.log('client svn contains project node? ' + (svnContainsProject ? 'Y' : 'N'));
        let rootpath = workspacePath;
        if (svnContainsProject && rootpath.endsWith('project')) {
            rootpath = path.join(rootpath, '..');
        }
        console.log('project root:' + rootpath);
        if (fs.existsSync(workspacePath)) {
            await toolchain.svnClient.cleanup(false, rootpath);
            await toolchain.svnClient.cleanup(false, workspacePath);
            await toolchain.svnClient.revert(rootpath);
            await toolchain.svnClient.update(rootpath);
        }
        else {
            fs.mkdirSync(rootpath, { recursive: true });
            await toolchain.svnClient.checkout(responsitory, rootpath);
        }
        console.timeEnd('SVN更新耗时');
    }
    async readBuiltInTpl() {
        if (toolchain.option.engine == 'unity') {
            const key = toolchain.option.platform == 'WebGL' ? toolchain.option.webglRuntime : toolchain.option.platform;
            const xf = path.join(toolchain.__dirname, '../templates/built_in_tpl.xlsx');
            const xbook = xlsx.readFile(xf);
            const xsheet = xbook.Sheets[key];
            if (xsheet != null) {
                const rows = xlsx.utils.sheet_to_json(xsheet, { blankrows: true });
                for (const r of rows) {
                    if (r.TEMPLATE == toolchain.option.executeTpl) {
                        return r;
                    }
                }
            }
        }
        console.error('could not find built-in template!');
        process.exit(1);
    }
    async getBuildConclusion() {
        return '';
    }
    async getAdditionalMessages() {
        return [];
    }
}
//# sourceMappingURL=Ancestor.js.map