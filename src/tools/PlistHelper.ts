import fs from "fs-extra"
import plist from "plist";

export enum ECertType {
    Invalid = '',
    Dev = 'development',
    AdHoc = 'ad-hoc',
    Store = 'app-store'
}

export interface IProvision {
    sign: string,
    provisioning: string,
    fakebundleid: string,
    companyName: string,
    team: string,
    provisioningUUID: string
}

export class PlistHelper {
    public static async read(file: string): Promise<plist.PlistValue> {
        const content = await fs.readFile(file, 'utf-8');
        return plist.parse(content);
    }

    public static async writeProvision(file: string, provisionName: string, bundleID: string, certType: ECertType): Promise<void> {
        const obj = {
            compileBitcode: false, 
            uploadSymbols: false,
            method: certType,
            provisioningProfiles: {
                buildleID: bundleID, 
                bundleID: provisionName
            }
        }
        await PlistHelper.write(file, obj);
    }

    public static async write(file: string, obj: plist.PlistValue): Promise<void> {
        const content = plist.build(obj);
        await fs.writeFile(file, content, 'utf-8');
    }
}
