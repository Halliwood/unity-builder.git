declare namespace unity {
    declare namespace meta {
        export declare type MetaTypes = Texture;
    
        export declare type TBuildTarget = 'DefaultTexturePlatform' | 'iPhone' | 'Standalone' | 'Server' | 'WebGL'
        
        export declare interface Texture {
            fileFormatVersion?: number;
            guid?: string;
            TextureImporter: {
                internalIDToNameTable?: any[];
                externalObjects?: {
                    [k: string]: any;
                };
                serializedVersion?: number;
                mipmaps?: {
                    mipMapMode?: number;
                    enableMipMap?: number;
                    sRGBTexture?: number;
                    linearTexture?: number;
                    fadeOut?: number;
                    borderMipMap?: number;
                    mipMapsPreserveCoverage?: number;
                    alphaTestReferenceValue?: number;
                    mipMapFadeDistanceStart?: number;
                    mipMapFadeDistanceEnd?: number;
                };
                bumpmap?: {
                    convertToNormalMap?: number;
                    externalNormalMap?: number;
                    heightScale?: number;
                    normalMapFilter?: number;
                };
                isReadable?: number;
                streamingMipmaps?: number;
                streamingMipmapsPriority?: number;
                grayScaleToAlpha?: number;
                generateCubemap?: number;
                cubemapConvolution?: number;
                seamlessCubemap?: number;
                textureFormat?: number;
                maxTextureSize?: number;
                textureSettings?: {
                    serializedVersion?: number;
                    filterMode?: number;
                    aniso?: number;
                    mipBias?: number;
                    wrapU?: number;
                    wrapV?: number;
                    wrapW?: number;
                };
                nPOTScale?: number;
                lightmap?: number;
                compressionQuality?: number;
                spriteMode?: number;
                spriteExtrude?: number;
                spriteMeshType?: number;
                alignment?: number;
                spritePivot?: {
                    x?: number;
                    y?: number;
                    [k: string]: any;
                };
                spritePixelsToUnits?: number;
                spriteBorder?: {
                    x?: number;
                    y?: number;
                    z?: number;
                    w?: number;
                };
                spriteGenerateFallbackPhysicsShape?: number;
                alphaUsage?: number;
                alphaIsTransparency?: number;
                spriteTessellationDetail?: number;
                textureType?: number;
                textureShape?: number;
                singleChannelComponent?: number;
                maxTextureSizeSet?: number;
                compressionQualitySet?: number;
                textureFormatSet?: number;
                applyGammaDecoding?: number;
                platformSettings: {
                    serializedVersion: number;
                    buildTarget: TBuildTarget;
                    maxTextureSize: number;
                    resizeAlgorithm: number;
                    textureFormat: number;
                    textureCompression: number;
                    compressionQuality: number;
                    crunchedCompression: number;
                    allowsAlphaSplitting: number;
                    overridden: number;
                    androidETC2FallbackOverride: number;
                    forceMaximumCompressionQuality_BC6H_BC7: number;
                }[];
                spriteSheet?: {
                    serializedVersion?: number;
                    sprites?: any[];
                    outline?: any[];
                    physicsShape?: any[];
                    bones?: any[];
                    spriteID?: null;
                    internalID?: number;
                    vertices?: any[];
                    indices?: null;
                    edges?: any[];
                    weights?: any[];
                    secondaryTextures?: any[];
                };
                spritePackingTag?: null;
                pSDRemoveMatte?: number;
                pSDShowRemoveMatteOption?: number;
                userData?: null;
                assetBundleName?: null;
                assetBundleVariant?: null;
            };
        }
    }
}
