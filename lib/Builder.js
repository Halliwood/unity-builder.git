import fs from 'fs-extra';
import fg from 'fast-glob';
import { checkDiskAvailable, gb2byte } from "./tools/vendor.js";
import { Ancestor } from './Ancestor.js';
import { PublishResTask } from './common/tasks/PublishResTask.js';
import { toolchain } from './toolchain.js';
import { SVNOperationTask } from './common/tasks/SVNOperationTask.js';
import { ReadCfgTask } from './common/tasks/ReadCfgTask.js';
import { PrepareI18nTask } from './common/tasks/PrepareI18nTask.js';
import { CompileCSharpTask } from './common/tasks/CompileCSharpTask.js';
import { BuildLock, checkOrLockWorkspace } from './tools/lock.js';
import { WrapCSharpTask } from './common/tasks/WrapCSharpTask.js';
import { CheckEnvTask } from './common/tasks/CheckEnvTask.js';
import { CheckCodeTask } from './common/tasks/CheckCodeTask.js';
import { uploadMedia } from './tools/alert.js';
import { SVNSaveTask } from './common/tasks/SVNSaveTask.js';
import { I18NTask } from './common/tasks/I18NTask.js';
import { CheckFileNameTask } from './common/tasks/CheckFileNameTask.js';
import { DynamicRemoveTask } from './common/tasks/DynamicRemoveTask.js';
import { ValidateTask } from './common/tasks/ValidateTask.js';
import path from 'path';
import { ModifyUnityTextureTask } from './common/tasks/ModifyUnityTextureTask.js';
import { SetScriptDefineTask } from './common/tasks/SetScriptDefineTask.js';
export class Builder extends Ancestor {
    async awake() {
        await this.getParams();
        return true;
    }
    async start() {
        // 打印参数
        // console.log('构建参数', toolchain.params);
        // 检查构建锁
        if (fs.existsSync(toolchain.params.workSpacePath)) {
            await checkOrLockWorkspace(BuildLock);
        }
        // 写入构建状态文件，此文件用于指示资源导入时识别是否处于构建抖音环境中
        await this.setBuildStateFile(true);
        // 检查磁盘
        await checkDiskAvailable(toolchain.params.workSpacePath, gb2byte(5));
        // 统一添加公共前置任务和后置任务
        this.series.unshift(new SVNOperationTask(this.cmdOption), new ReadCfgTask(this.cmdOption), new DynamicRemoveTask(this.cmdOption), new SetScriptDefineTask(this.cmdOption), new ModifyUnityTextureTask(this.cmdOption), new CheckCodeTask(this.cmdOption), new CheckFileNameTask(this.cmdOption), new ValidateTask(this.cmdOption), new CheckEnvTask(this.cmdOption), new PrepareI18nTask(this.cmdOption), new CompileCSharpTask(this.cmdOption), new WrapCSharpTask(this.cmdOption));
        this.series.push(new PublishResTask(this.cmdOption), new SVNSaveTask(this.cmdOption));
        // 开始执行所有任务
        await this.startSeries();
        // 清除构建状态文件
        await this.setBuildStateFile(false);
        // 保存构建信息到.build/buildInfo.json中
        await this.saveBuildInfo();
    }
    async setBuildStateFile(setOrNot) {
        const bsfRoot = path.join(toolchain.params.workSpacePath, `../.build`);
        const bsfs = await fg('*.building', { cwd: bsfRoot });
        for (const bsf of bsfs) {
            console.log('remove build state file:', bsf);
            await fs.unlink(path.join(bsfRoot, bsf));
        }
        if (setOrNot) {
            let state = toolchain.option.platform;
            if (state == 'WebGL') {
                state += `_${toolchain.option.webglRuntime}`;
            }
            const bsfPath = path.join(bsfRoot, `${state.toLowerCase()}.building`);
            console.log('set build state file:', bsfPath);
            await fs.ensureFile(bsfPath);
        }
    }
    async saveBuildInfo() {
        const buildInfoFile = path.join(toolchain.params.workSpacePath, `../.build/buildInfo.json`);
        let book;
        if (fs.existsSync(buildInfoFile)) {
            book = await fs.readJSON(buildInfoFile);
        }
        else {
            await fs.ensureFile(buildInfoFile);
            book = {};
        }
        const fbv = await toolchain.unity.getFullBuildVer();
        book[fbv] = toolchain.buildInfo;
        await fs.writeJSON(buildInfoFile, book, { spaces: 2 });
    }
    async getBuildConclusion() {
        const ver = await toolchain.unity.getFullBuildVer();
        let str = `版本号：${ver}`;
        if (this.cmdOption.platform == 'WebGL') {
            if (this.cmdOption.webglRuntime == 'browser') {
                str += '  `网页版`';
            }
            else if (this.cmdOption.webglRuntime == 'minigame') {
                str += '  `小游戏`';
            }
            else if (this.cmdOption.webglRuntime == 'douyin') {
                str += '  `抖音`';
            }
        }
        if (this.cmdOption.buildExe)
            str += `  \`新包(${toolchain.option.gameIds})\``;
        return str;
    }
    async getAdditionalMessages() {
        const msgs = [];
        const i18nRst = toolchain.taskHelper.getResult(I18NTask.name);
        if (i18nRst != null) {
            const report = i18nRst.data?.out?.report;
            if (report) {
                if (report.jsonSafeErrors.length > 0)
                    msgs.push({
                        type: 'text',
                        msg: 'JSON错误将导致无法进入游戏，请优先解决',
                        mention: toolchain.params.channelCfg['localize-responsor']
                    });
            }
        }
        if (toolchain.params.reportLvs.includes('JsonDiff')) {
            const svnRst = toolchain.taskHelper.getResult(SVNOperationTask.name);
            if (svnRst != null && svnRst.data) {
                // 上传diff文件到企业微信
                let mediaId = null;
                if (svnRst.data.jsonDiff) {
                    mediaId = await uploadMedia(svnRst.data.jsonDiff);
                }
                const diffJsonNames = svnRst.data?.diffJsonNames;
                if (diffJsonNames?.length) {
                    let msg = `本次构建更新了${diffJsonNames.length}个配置：${diffJsonNames.join(', ')}`;
                    if (mediaId) {
                        msg += '，具体查看下面的diff文件：';
                    }
                    msgs.push({
                        type: 'text',
                        msg,
                        mention: toolchain.startUser
                    });
                }
                if (mediaId) {
                    msgs.push({
                        type: 'file',
                        file: mediaId
                    });
                }
            }
        }
        return msgs;
    }
}
//# sourceMappingURL=Builder.js.map