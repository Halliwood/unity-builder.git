import { CmdOption } from "../CmdOption.js";
import { Cmd } from "./Cmd.js";
import { BuildParams } from "../BuildParams.js";
export declare class UnityHelper {
    private fullVersion?;
    private _lastErrCode;
    get lastErrCode(): number;
    runUnityCommand(cmdOption: CmdOption, params: string[], tag?: string, ignoreError?: boolean, outputStripper?: (raw: string) => string): Promise<Cmd>;
    getUnityInfo(params: BuildParams): Promise<void>;
    private identifyUnityVersion;
    decideAndroidEnv(): Promise<void>;
    getUnityLogFile(): string;
    supportCompressTex(): boolean;
    getAssetsDir(): string;
    getCDNURL(): string;
    isPuertsInstalledViaLocalUPM(): boolean;
    getPuertsInstallRoot(): string;
    getPuertsCommonjsInstallRoot(): string;
    commitFullBuildVer(ver: string): Promise<void>;
    getResVersion(): Promise<number>;
    /**获取本次构建相关的完整版本号 */
    getFullBuildVer(): Promise<string>;
    readGUID(metaFile: string): Promise<string>;
    /**
     * 查询所有AB包文件的版本号信息
     * @param files
     * @returns
     */
    getABFileMD5s(): Promise<Record<string, string>>;
}
