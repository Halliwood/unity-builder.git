import { CmdOption } from '../../CmdOption.js';
import { TaskResult } from '../../common/tasks/BastTask.js';
import { IBuildAppResult } from '../../typings.js';
export declare class AndroidBuildLaya {
    static run(cmdOption: CmdOption): Promise<TaskResult<IBuildAppResult>>;
}
