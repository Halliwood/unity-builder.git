import { Ancestor } from '../Ancestor.js';
export declare abstract class LayaBaseBuilder extends Ancestor {
    awake(): Promise<boolean>;
    start(): Promise<void>;
}
