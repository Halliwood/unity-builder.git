import fs from 'fs-extra';
import path from 'path';
import { toolchain } from "../../toolchain.js";
import { BaseTask, TaskResult } from "./BastTask.js";

export declare interface SVNInfo {
    json: number
}

/**
 * 保存本次SVN版本号信息。
 */
export class SVNSaveTask extends BaseTask {
    async run(): Promise<TaskResult<void>> {
        if (toolchain.option.engine != 'laya') {
            // 目前只支持unity项目
            const jsonRoot = path.join(toolchain.params.workSpacePath, 'Assets/AssetSources/data');
    
            const jsonRevision = await toolchain.svnClient.getRevision(jsonRoot);
            console.log('json revision:', jsonRevision);

            const svnInfoFile = path.join(toolchain.params.workSpacePath, `../.build/svnInfo.json`);
            await fs.ensureFile(svnInfoFile);
            const info: SVNInfo = { json: jsonRevision };
            await fs.writeJSON(svnInfoFile, info, { spaces: 2 });
        }

        return { success: true, errorCode: 0 };
    }
}
