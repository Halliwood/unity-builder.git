import { toolchain } from "../../toolchain.js";
import { PkgType } from "../../typings";
import { BaseTask, TaskResult } from "./BastTask.js";

export abstract class BaseMakeHtmlTask extends BaseTask {
    protected async makeHtml(pkgType: PkgType, patterns: string[]): Promise<TaskResult<void>> {
        await toolchain.htmlMaker.exportPkgsListHtml(toolchain.params.packageLocalWebPath, this.cmdOption, toolchain.params, toolchain.platformCfgParser, pkgType, patterns);

        return { success: true, errorCode: 0 };
    }
}
