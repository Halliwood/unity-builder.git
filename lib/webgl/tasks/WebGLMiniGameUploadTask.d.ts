import { BaseTask, TaskResult } from "../../common/tasks/BastTask.js";
import { UploadResult } from "../../tools/WechatTools.js";
import { Nullable } from "../../typings.js";
export interface WebGLMiniGameUploadData {
    success: boolean;
    result: UploadResult | null;
}
export declare class WebGLMiniGameUploadTask extends BaseTask<WebGLMiniGameUploadData> {
    run(): Promise<TaskResult<WebGLMiniGameUploadData>>;
    /**
     * copy ts的map到资源服务器上
     **/
    private copyTsscriptMaps;
    get dependencies(): Nullable<string[]>;
}
