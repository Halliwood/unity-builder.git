import { Builder } from "../Builder.js";
import { PCBuildUnityTask } from './tasks/PCBuildUnityTask.js';
import { PCMakeHtmlTask } from "./tasks/PCMakeHtmlTask.js";
import { BuildAssetsTask } from "../common/tasks/BuildAssetsTask.js";
import { PreBuildAppTask } from "../common/tasks/PreBuildAppTask.js";
import { I18NTask } from "../common/tasks/I18NTask.js";
import { UnityTscTask } from "../common/tasks/UnityTscTask.js";
export class PCBuilder extends Builder {
    cmdOption;
    constructor(cmdOption) {
        super(cmdOption);
        this.cmdOption = cmdOption;
        this.series.push(new I18NTask(this.cmdOption), new UnityTscTask(this.cmdOption), new BuildAssetsTask(this.cmdOption), new PreBuildAppTask(this.cmdOption), new PCBuildUnityTask(this.cmdOption), new PCMakeHtmlTask(this.cmdOption));
    }
}
//# sourceMappingURL=PCBuilder.js.map