import { toolchain } from "../../toolchain.js";
import { BaseTask, TaskResult } from "./BastTask.js";
import { WechatTools } from "../../tools/WechatTools.js";

export class CheckEnvTask extends BaseTask {
    async run(): Promise<TaskResult<void>> {
        // 微信小游戏需要检查开发者工具
        if (this.cmdOption.webglRuntime == 'minigame') {
            // 获取开发者工具目录
            await WechatTools.getDevtools();
            if (!toolchain.wechatDevtools) {
                console.error('Cannot get wechat devtools location!');
                return { success: false, errorCode: 1 };
            }
            console.log('wechat devtools:', toolchain.wechatDevtools);
            // 检查是否已登录开发者工具
            const isloginRet = await WechatTools.islogin();
            if (!isloginRet.data) {
                console.error('wechat devtools is not login!');
                // 请求登录二维码并发送到群聊
                await WechatTools.login();
                return { success: false, errorCode: 1 };
            }
        }

        return { success: true, errorCode: 0 };
    }
}
