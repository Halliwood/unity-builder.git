import { BaseTask, TaskResult } from '../../common/tasks/BastTask.js';
export declare class LayaUploadResTask extends BaseTask {
    run(): Promise<TaskResult<void>>;
    private upLoadRes;
    get skip(): boolean;
}
