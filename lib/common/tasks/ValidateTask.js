import { toolchain } from "../../toolchain.js";
import { BaseTask } from "./BastTask.js";
import fs from "fs-extra";
import path from "path";
import { Cmd } from "../../tools/Cmd.js";
import { sendBuildFailureAlert, sendRobotMsg } from "../../tools/alert.js";
export class ValidateTask extends BaseTask {
    async run() {
        const btRoot = path.join(toolchain.params.workSpacePath, 'build-tools');
        const vjs = path.join(btRoot, 'dist/Validate.js');
        let code = [];
        if (fs.existsSync(vjs)) {
            const cmd = new Cmd();
            let failed = false;
            const errorCode = await cmd.run('node', [vjs]).catch(async (e) => {
                failed = true;
            });
            if (errorCode != 0) {
                failed = true;
            }
            if (failed) {
                await sendRobotMsg('text', '资源校验不通过，请修改。\n' + cmd.errorOutput);
                await sendBuildFailureAlert('资源校验不通过。');
                return { success: false, errorCode: 1 };
            }
        }
        return { success: true, errorCode: 0 };
    }
}
//# sourceMappingURL=ValidateTask.js.map