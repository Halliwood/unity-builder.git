import { BaseTask, TaskResult } from "./BastTask.js";
export declare class PrepareI18nTask extends BaseTask {
    run(): Promise<TaskResult<void>>;
}
