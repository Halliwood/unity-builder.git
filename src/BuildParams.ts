import { ConnectConfig } from "ssh2";
import { ChannelCfg, HostCfg, HostCommonCfg, ProjectCfg, ProjectGlobalCfg } from "./typings";

export declare type ProjectType = 'develop' | 'publish' | 'publishfix';
export declare type TReportLv = 'JsonDiff';

export interface ResPublishParams {
    verJsonPath: string;
    /**此字段仅unity项目使用 */
    srcDirPath: string;
    tmpResDir: string;
    srcResfile: string;
    dstResfile: string;
    /**sourcemap资源外发的字段 */
    tmpSrcMapDir: string;
    srcSrcMapfile: string;
    dstSrcMapfile: string;
}

export interface BuildParams {
    unityVer: string;
    hostCfg: HostCfg;
    hostProjCfg: HostCommonCfg;
    skipTasks?: string[];
    executeTasks?: string[];
    reportLvs: TReportLv[];
    projCfg: ProjectCfg;
    ip: string;
    projectName: string;
    projectType: ProjectType;
    platformPath: 'platform' | 'platformIos';
    androidSdkRoot: string;
    androidSdkRoot_unity563?: string;
    /**本地资源上传路径 */
    uploadPath: string;
    version: string;
    developProjectPath?: string;
    packageFolder: string;
    packageLocalWebPath: string;
    workSpacePath: string;
    defines?: string;
    unityTool: string | undefined;
    androidNdkRoot: string;
    macHost: string;
    macPort: number;
    macUsername: string;
    macPassword: string;
    macWorkspace: string;
    needsetEditorPrefs: boolean;
    qrcodeUrl: string;
    svn: {
        urlMap: {
            code: {
                develop: string;
                developTag: string;
                publish: string;
                publishTag: string;
                publishfix: string;
                publishfixTag: string;
            }, 
            xls: {
                develop: string;
                developTag: string;
                publish: string;
                publishTag: string;
                publishfix: string;
                publishfixTag: string;
            }
        }, 
        userInfo: string;
    }, 
    projGlobalCfg?: ProjectGlobalCfg;
    channelCfg: ChannelCfg;
    allLangs: string[];
    BuildPrivateOBB: boolean;
    /**此字段仅unity项目使用，laya项目规则不同勿用 */
    unityResPublishCfg: ResPublishParams;
    /**资源外发机器信息 */
    cdnHostInfo: {
        hostinfo: ConnectConfig;
        cdnDir: string;

        srcMapHostinfo: ConnectConfig;
        srcMapCdnDir: string;
    }
}