import { Ancestor } from './Ancestor.js';
export declare class HudsonJob extends Ancestor {
    private jobOpt;
    awake(): Promise<boolean>;
    start(): Promise<void>;
    private makeBranch;
    private makeBuildJob;
    private readTemplate;
    private writeJob;
}
