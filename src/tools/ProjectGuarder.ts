import fg from 'fast-glob';
import path from 'path';
import { EFileErrorType, alertErrorFile, sendBuildFailureAlert } from './alert.js';

export class ProjectGuarder {
    public async checkUnity(projRoot: string): Promise<void> {
        const assetsRoot = path.join(projRoot, 'Assets')
        const repeatDots = await fg(['**/*..png', '**/*..jpg', '**/*..tga'], { cwd: assetsRoot });
        const msgs: string[] = [];
        if (repeatDots.length > 0) {
            const fileStr = repeatDots.map(v => path.basename(v)).join(', ');
            msgs.push(`\`多余的.(${repeatDots.length})\`: ${fileStr}`);
            await alertErrorFile(path.join(assetsRoot, repeatDots[0]), EFileErrorType.FileNameError, fileStr);
        }
        if (msgs.length > 0) {
            await sendBuildFailureAlert('以下资源命名错误：' + msgs.join('\n'));
        }
    }
}
