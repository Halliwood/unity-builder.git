import { Builder } from "../Builder.js";
import { CmdOption } from "../CmdOption.js";
/**
 * WebGL全平台构建器
 */
export declare class WebGLAllBuilder extends Builder {
    protected cmdOption: CmdOption;
    constructor(cmdOption: CmdOption);
}
