import fs from "fs-extra";
import path from "path";
import { BaseTask, TaskResult } from "../../common/tasks/BastTask.js";
import { toolchain } from '../../toolchain.js';
import { Cmd } from '../../tools/Cmd.js';

/**Laya编译ts */
export class LayaCompileTask extends BaseTask<void> {
    async run(): Promise<TaskResult<void>> {
        process.chdir(toolchain.params.workSpacePath);
        // 先清空bin/js
        const binJs = path.join(toolchain.params.workSpacePath, 'bin/js');
        if (fs.existsSync(binJs)) await fs.emptyDir(binJs);
        const tsconfig = this.cmdOption.platform == 'web' ? 'tsconfig.json' : 'tsconfig.wxgame.json';
        await new Cmd().runNodeModule('tsc', ['-p', tsconfig]);
        return { success: true, errorCode: 0, data: void 0 };
    }
}
