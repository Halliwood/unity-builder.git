import { TaskResult } from "./common/tasks/BastTask.js";
export declare class TaskHelper {
    private taskResults;
    saveResult(name: string, result: TaskResult): void;
    getResult<T>(name: string): TaskResult<T> | null;
}
