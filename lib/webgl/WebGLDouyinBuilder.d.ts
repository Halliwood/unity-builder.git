import { Builder } from "../Builder.js";
import { CmdOption } from "../CmdOption.js";
/**
 * WebGL抖音构建器
 */
export declare class WebGLDouYinBuilder extends Builder {
    protected cmdOption: CmdOption;
    constructor(cmdOption: CmdOption);
}
