import { Builder } from "../Builder.js";
import { WebGLInstallTask } from "./tasks/WebGLInstallTask.js";
import { WebGLBuildMiniGameTask } from "./tasks/WebGLBuildMiniGameTask.js";
import { WebGLPostBuildMiniGameTask } from "./tasks/WebGLPostBuildMiniGameTask.js";
import { WebGLBuildBrowserTask } from "./tasks/WebGLBuildBrowserTask.js";
import { WebGLPostBuildBrowserTask } from "./tasks/WebGLPostBuildBrowserTask.js";
import { BuildAssetsTask } from "../common/tasks/BuildAssetsTask.js";
import { PreBuildAppTask } from "../common/tasks/PreBuildAppTask.js";
import { WebGLCompressTextureTask } from "./tasks/WebGLCompressTextureTask.js";
import { WebGLCheckCfgTask } from "./tasks/WebGLCheckCfgTask.js";
import { I18NTask } from "../common/tasks/I18NTask.js";
import { UnityTscTask } from "../common/tasks/UnityTscTask.js";
/**
 * WebGL全平台构建器
 */
export class WebGLAllBuilder extends Builder {
    cmdOption;
    constructor(cmdOption) {
        super(cmdOption);
        this.cmdOption = cmdOption;
        this.series.push(new WebGLCheckCfgTask(this.cmdOption), new WebGLInstallTask(this.cmdOption), new I18NTask(this.cmdOption), new UnityTscTask(this.cmdOption), new BuildAssetsTask(this.cmdOption), new PreBuildAppTask(this.cmdOption), new WebGLBuildBrowserTask(this.cmdOption), new WebGLPostBuildBrowserTask(this.cmdOption), new WebGLBuildMiniGameTask(this.cmdOption), new WebGLCompressTextureTask(this.cmdOption), new WebGLPostBuildMiniGameTask(this.cmdOption));
    }
}
//# sourceMappingURL=WebGLAllBuilder.js.map