import { BaseTask, TaskResult } from "./BastTask.js";
/**
 * 保存本次SVN版本号信息。
 */
export declare class SetScriptDefineTask extends BaseTask {
    run(): Promise<TaskResult<void>>;
}
