import { BaseMakeHtmlTask } from "../../common/tasks/BaseMakeHtmlTask.js";
import { TaskResult } from "../../common/tasks/BastTask.js";

export class AndroidMakeHtmlTask extends BaseMakeHtmlTask {
    async run(): Promise<TaskResult<void>> {
        return await this.makeHtml('apk', ['*.apk', '*.aab']);
    }
}
