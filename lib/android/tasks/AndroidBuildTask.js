import { BaseTask } from "../../common/tasks/BastTask.js";
import { PreBuildAppTask } from "../../common/tasks/PreBuildAppTask.js";
import { WrapCSharpTask } from "../../common/tasks/WrapCSharpTask.js";
import { AndroidBuildLaya } from "./AndroidBuildLaya.js";
import { AndroidBuildUnity } from "./AndroidBuildUnity.js";
export class AndroidBuildTask extends BaseTask {
    async run() {
        if (this.cmdOption.engine == "laya") {
            return await AndroidBuildLaya.run(this.cmdOption);
        }
        else {
            return await AndroidBuildUnity.run(this.cmdOption);
        }
    }
    get dependencies() {
        if (this.cmdOption.engine == "laya") {
            return [];
        }
        else {
            return [WrapCSharpTask.name, PreBuildAppTask.name];
            ;
        }
    }
    get skip() {
        return !this.cmdOption.buildExe;
    }
}
//# sourceMappingURL=AndroidBuildTask.js.map