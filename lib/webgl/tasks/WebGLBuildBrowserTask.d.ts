import { TaskResult } from "../../common/tasks/BastTask.js";
import { BuildUnityResult, WebGLBuildUnityBaseTask } from "./WebGLBuildUnityBaseTask.js";
export declare class WebGLBuildBrowserTask extends WebGLBuildUnityBaseTask {
    run(): Promise<TaskResult<BuildUnityResult>>;
}
