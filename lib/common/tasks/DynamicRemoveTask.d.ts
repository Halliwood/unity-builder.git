import { BaseTask, TaskResult } from "./BastTask.js";
export declare class DynamicRemoveTask extends BaseTask {
    run(): Promise<TaskResult<void>>;
    private executeRemove;
}
