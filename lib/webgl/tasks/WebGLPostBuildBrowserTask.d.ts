import { TaskResult } from "../../common/tasks/BastTask.js";
import { WebGLProcessJsTask } from "./WebGLPostBuildUnityTask.js";
export declare class WebGLPostBuildBrowserTask extends WebGLProcessJsTask {
    run(): Promise<TaskResult<void>>;
    private tryAppendMd5;
    private maxExtname;
    private replaceFileMd5;
}
