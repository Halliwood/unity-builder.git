import crypto from 'crypto';
import { CopyOptions } from 'fs-extra';
import archiver from 'archiver';
export declare type FindFileMode = '+' | '-';
export declare function checkDiskAvailable(p: string, minByte: number): Promise<void>;
export declare function gb2byte(gb: number): number;
export declare function fileSize(file: string): Promise<number>;
export declare function eitherPath(a: string, b: string): string;
export declare function safeCopy(src: string, dest: string, options?: CopyOptions): Promise<void>;
export declare function safeCopyFile(src: string, dest: string): Promise<void>;
export declare function findFiles(root: string, exts: string[], mode: FindFileMode): Promise<string[]>;
export declare function joinURLs(...urls: string[]): string;
export declare function download(url: string, output: string): Promise<number>;
/**附加md5并返回新的文件名 */
export declare function appendMd5(file: string, md5Len: number, ext?: string, connectChar?: string): Promise<{
    fileMd5: string;
    newFileName: string;
}>;
export declare function replaceOrThrow(s: string, searchValue: string | RegExp, replaceValue: string): string;
export declare function md5(str: crypto.BinaryLike): string;
export declare function replaceInFileOrThrow(file: string, searchValue: string | RegExp, replaceVale: string): Promise<void>;
export declare function writeUTF8withBom(file: string, content: string): void;
export declare function readNumber(file: string): Promise<number>;
/**
 * 制作压缩包
 * @param targetName 生成的压缩包名字/路径
 * @param format 压缩格式
 * @param source 压缩包内容源目录，默认为当前目录，源目录底下所有文件将被打包到压缩包中的同名文件夹下
 */
export declare function makeArchive(targetName: string, format: archiver.Format, source?: string): Promise<void>;
export declare function unzipTo(dirname: string, zipfilename: string): Promise<void>;
export declare function zipTo(outaarpath: string, tmppath: string): Promise<void>;
export declare function gzip(inputFile: string, outputFile: string): Promise<void>;
export declare function getModifyTime(file: string): string;
export declare function findOption(options: string[], optionName: string): string | null;
/**设置一组选项，并返回新选项数组。不影响原选项数组。 */
export declare function setOption(options: string[], optionName: string, optionValue: string): string[];
/**
 * 读取version.txt并解析内容。大部分项目的version.txt仅包含资源版本号，新版本的小游戏项目（比如斗破小游戏）还包含了json版本号。
 * @param txtFile version.txt文件路径
 * @returns 一个形如[资源版本号, json版本号]的数组。
 */
export declare function readVersionTxt(txtFile: string): Promise<[verCode: string, jsonMd5?: string]>;
