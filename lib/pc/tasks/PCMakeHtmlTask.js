import { BaseMakeHtmlTask } from "../../common/tasks/BaseMakeHtmlTask.js";
export class PCMakeHtmlTask extends BaseMakeHtmlTask {
    async run() {
        return await this.makeHtml('exe', ['*.zip']);
    }
}
//# sourceMappingURL=PCMakeHtmlTask.js.map