import { TaskResult } from "./common/tasks/BastTask.js";

export class TaskHelper {
    private taskResults: { [name: string]: TaskResult<any> } = {};

    public saveResult(name: string, result: TaskResult): void {
        this.taskResults[name]= result;
    }

    public getResult<T>(name: string): TaskResult<T> | null {
        return this.taskResults[name];
    }
}
