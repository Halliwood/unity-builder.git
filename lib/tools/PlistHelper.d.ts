import plist from "plist";
export declare enum ECertType {
    Invalid = "",
    Dev = "development",
    AdHoc = "ad-hoc",
    Store = "app-store"
}
export interface IProvision {
    sign: string;
    provisioning: string;
    fakebundleid: string;
    companyName: string;
    team: string;
    provisioningUUID: string;
}
export declare class PlistHelper {
    static read(file: string): Promise<plist.PlistValue>;
    static writeProvision(file: string, provisionName: string, bundleID: string, certType: ECertType): Promise<void>;
    static write(file: string, obj: plist.PlistValue): Promise<void>;
}
