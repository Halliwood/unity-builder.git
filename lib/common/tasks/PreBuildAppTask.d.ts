import { BaseTask, TaskResult } from "./BastTask.js";
export interface PrebuildAppData {
    defines: string;
    versionCode: string;
}
export declare class PreBuildAppTask extends BaseTask<PrebuildAppData> {
    run(): Promise<TaskResult<PrebuildAppData>>;
    private _createChannelScriptByCfg;
    private createBuildDefinesScript;
    private _restoreBuildinTxtFile;
    private confuseAll;
    private copyBuildinAssets;
    private _addSpecAB2BuildInList;
    get skip(): boolean;
}
