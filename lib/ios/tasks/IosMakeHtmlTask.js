import { BaseMakeHtmlTask } from "../../common/tasks/BaseMakeHtmlTask.js";
export class IosMakeHtmlTask extends BaseMakeHtmlTask {
    async run() {
        return await this.makeHtml('ipa', ['*.ipa', '*.aab']);
    }
}
//# sourceMappingURL=IosMakeHtmlTask.js.map