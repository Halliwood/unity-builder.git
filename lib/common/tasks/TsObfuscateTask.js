import { toolchain } from "../../toolchain.js";
import { BaseTask } from "./BastTask.js";
import { JsonTerser } from "../../tools/JsonTerser.js";
export class TsObfuscateTask extends BaseTask {
    async run() {
        // 混淆json
        if (toolchain.params.channelCfg.terserJson) {
            const jt = new JsonTerser();
            await jt.run(toolchain.params.workSpacePath);
        }
        return { success: true, errorCode: 0 };
    }
}
//# sourceMappingURL=TsObfuscateTask.js.map