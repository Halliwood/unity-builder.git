import { toolchain } from "../../toolchain.js";
import { BaseTask } from "./BastTask.js";
export class BaseMakeHtmlTask extends BaseTask {
    async makeHtml(pkgType, patterns) {
        await toolchain.htmlMaker.exportPkgsListHtml(toolchain.params.packageLocalWebPath, this.cmdOption, toolchain.params, toolchain.platformCfgParser, pkgType, patterns);
        return { success: true, errorCode: 0 };
    }
}
//# sourceMappingURL=BaseMakeHtmlTask.js.map