import { Builder } from '../Builder.js';
import { PreBuildAppTask } from '../common/tasks/PreBuildAppTask.js';
import { BuildAssetsTask } from '../common/tasks/BuildAssetsTask.js';
import { AndroidMakeHtmlTask } from './tasks/AndroidMakeHtmlTask.js';
import { I18NTask } from '../common/tasks/I18NTask.js';
import { AndroidBuildTask } from './tasks/AndroidBuildTask.js';
import { AndroidCheckNewApkTask } from './tasks/AndroidCheckNewApkTask.js';
import { toolchain } from '../toolchain.js';
import { UnityTscTask } from '../common/tasks/UnityTscTask.js';
export class AndroidBuilder extends Builder {
    cmdOption;
    constructor(cmdOption) {
        super(cmdOption);
        this.cmdOption = cmdOption;
        this.series.push(new I18NTask(this.cmdOption), new UnityTscTask(this.cmdOption), new BuildAssetsTask(this.cmdOption), new PreBuildAppTask(this.cmdOption), new AndroidBuildTask(this.cmdOption), new AndroidMakeHtmlTask(this.cmdOption), new AndroidCheckNewApkTask(this.cmdOption));
    }
    async getBuildConclusion() {
        if (this.cmdOption.engine == "laya") {
            return "";
        }
        else {
            return await super.getBuildConclusion();
        }
    }
    async getAdditionalMessages() {
        const msgs = await super.getAdditionalMessages();
        let apkUrl = null;
        if (toolchain.option.buildExe && toolchain.option.setMinApp) {
            const appRst = toolchain.taskHelper.getResult(AndroidBuilder.name);
            if (appRst?.data) {
                apkUrl = toolchain.htmlMaker.apkUrlMapper[appRst.data.apkName];
                if (!apkUrl) {
                    console.error('no apk url for: ' + appRst.data.apkName);
                }
            }
        }
        const rst = toolchain.taskHelper.getResult(AndroidCheckNewApkTask.name);
        if (rst?.data?.needNewApk) {
            if (!toolchain.option.buildExe) {
                msgs.push({
                    type: 'text',
                    msg: `警告：预制体${rst.data.hitPrefab}引用了新脚本${rst.data.hitCS}，需要构新apk。\n本次构建没有构新apk，游戏将无法正常运行。`,
                    mention: '@all'
                });
            }
            else {
                let msg = `警告：预制体${rst.data.hitPrefab}引用了新脚本${rst.data.hitCS}，需要安装新apk，否则游戏将无法正常运行。`;
                if (apkUrl) {
                    msg += `\n点击下载新包：${apkUrl}`;
                }
                msgs.push({
                    type: 'text',
                    msg,
                    mention: '@all'
                });
            }
        }
        else {
            if (toolchain.option.buildExe && toolchain.option.setMinApp && apkUrl) {
                const msg = `老包已失效，点击下载新包：${apkUrl}`;
                msgs.push({
                    type: 'text',
                    msg,
                    mention: '@all'
                });
            }
        }
        return msgs;
    }
}
//# sourceMappingURL=AndroidBuilder.js.map