import { BaseTask, TaskResult } from "./BastTask.js";
import { I18NOut } from "../I18N.js";
export declare interface I18NResult {
    out: I18NOut | null;
}
export declare class I18NTask extends BaseTask<I18NResult> {
    run(): Promise<TaskResult<I18NResult>>;
    private _prepare_localize_res;
    private modifyLangCfg;
}
