import { CmdOption } from "../CmdOption.js";
import { LayaBaseBuilder } from "./LayaBaseBuilder.js";
export declare class LayaWxgameBuilder extends LayaBaseBuilder {
    protected cmdOption: CmdOption;
    constructor(cmdOption: CmdOption);
}
