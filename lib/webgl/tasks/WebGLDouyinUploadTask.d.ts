import { BaseTask, TaskResult } from "../../common/tasks/BastTask.js";
import { UploadResult } from "../../tools/WechatTools.js";
import { Nullable } from "../../typings.js";
export interface WebGLDouyinUploadData {
    success: boolean;
    result: UploadResult | null;
}
export declare class WebGLDouyinUploadTask extends BaseTask<WebGLDouyinUploadData> {
    run(): Promise<TaskResult<WebGLDouyinUploadData>>;
    get dependencies(): Nullable<string[]>;
}
