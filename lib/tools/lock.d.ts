export interface ILockFile {
    file: 'buildlock' | 'i18nlock';
    tip: string;
}
export declare const BuildLock: ILockFile;
export declare const I18NLock: ILockFile;
export declare function checkOrLockWorkspace(lock: ILockFile): Promise<void>;
export declare function unlockWorkspace(): Promise<void>;
