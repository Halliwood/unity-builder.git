import { ITerserCache } from "../typings";
export declare class JsTerser {
    terserAll(dir: string, cache: ITerserCache): Promise<void>;
}
