/**
 * 默认环境变量。新版unity-builder不再需要配置hostinfo.yml，并通过各项目的xxx.ini结合下述环境变量自动生成构建配置。
 * 如需修改默认环境变量，你可以在Hudson或jenkins后台设置下述环境变量。
 */
export declare const env: {
    projIniRoot: string;
    defaultWorkspace: string;
    defaultResRoot: string;
    defaultNDK: string;
    /**MAC设备信息，格式：IP:端口@用户名,密码:工作目录 */
    defaultMac: string;
    ChatSvr: string;
    clientGroupChatID: string;
    clientGroupMention: string;
    svnUser: string;
    svnPswd: string;
    ttMail: string;
    ttPswd: string;
};
export declare const fillEnvValues: () => void;
