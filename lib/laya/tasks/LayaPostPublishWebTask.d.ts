import { TaskResult } from "../../common/tasks/BastTask.js";
import { Nullable } from "../../typings.js";
import { LayaPostPublishBaseTask } from "./LayaPostPublishBaseTask.js";
export declare class LayaPostPublishWebTask extends LayaPostPublishBaseTask {
    run(): Promise<TaskResult<void>>;
    get dependencies(): Nullable<string[]>;
}
