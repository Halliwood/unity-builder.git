import { CmdOption } from "../CmdOption.js";
import { BuildParams } from "../BuildParams.js";
export declare class HostInfoParser {
    getBuildCfg(option: CmdOption, params: BuildParams): Promise<void>;
    private makeLayaBuildNodeCfg;
    private makeUnityBuildNodeCfg;
    private makeUnityPlatCfg;
    /**获取总test机信息 */
    private getNewTestHost;
    /** 获取webbugly机信息 */
    private getWebBuglyHost;
    private getCurCfg;
}
