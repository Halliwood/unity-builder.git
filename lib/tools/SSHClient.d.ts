import { ConnectConfig } from 'ssh2';
export declare class SSHClient {
    private conn;
    private os;
    private cmdStdout;
    connect(hostinfo: ConnectConfig): Promise<number>;
    exec(cmd: string, silent?: boolean): Promise<string>;
    put(localPath: string, remotePath: string): Promise<number>;
    get(remotePath: string, localPath: string): Promise<number>;
    end(): Promise<number>;
}
