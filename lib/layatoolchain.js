import path from "path";
import { toolchain } from "./toolchain.js";
const vars = { publishRoot: '', buildConfigMap: {}, fileNameMapper: {}, md5Map: {}, patchMap: {} };
export const layatoolchain = {
    vars,
    commonFileRoot: () => path.join(toolchain.params.workSpacePath, 'build/projectfiles/common'),
    projectFilesRoot: (pf, type) => path.join(toolchain.params.workSpacePath, 'build/projectfiles', pf, type),
    newRecRoot: (oldToolsRoot) => path.join(oldToolsRoot, 'h5Build/configs', '__new'),
    projPublishRecRoot: (oldToolsRoot) => path.join(oldToolsRoot, 'h5Build/configs', toolchain.params.projectName),
    pfPublishRecRoot: (oldToolsRoot, pf) => path.join(oldToolsRoot, 'h5Build/configs', toolchain.params.projectName, pf, toolchain.params.projectType)
};
//# sourceMappingURL=layatoolchain.js.map