import { Builder } from "../Builder.js";
import { CmdOption } from "../CmdOption.js";
export declare class IosBuilder extends Builder {
    protected cmdOption: CmdOption;
    constructor(cmdOption: CmdOption);
}
