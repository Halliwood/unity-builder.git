export declare class Hudson {
    static getHudsonHome(): string;
    static getBuildNumber(): string;
    static getBuildID(): string;
    static getBuildURL(): string;
    static getJobName(): string;
    static getStartUser(): Promise<string>;
}
