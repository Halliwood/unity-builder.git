import { BaseTask, TaskResult } from "./BastTask.js";
export declare class ValidateTask extends BaseTask {
    run(): Promise<TaskResult<void>>;
}
