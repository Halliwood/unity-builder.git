import { BaseTask, TaskResult } from "./BastTask.js";
export declare class BuildAssetsTask extends BaseTask {
    run(): Promise<TaskResult<void>>;
    private _prepare_majiabao_res;
    private _copyChannelAssets;
    private _copyChannelAssetsToMain;
    private _modifyManifestFile;
    private _buildJsonData;
    private _createAssetBundle;
    private _buildRawRes;
    private checkCmdOutput;
    private _deleteUnityLibraryMetas;
    private _backBuildinTxtFile;
    /**
     * copy ts的map到资源服务器上
     **/
    private copyTsscriptMaps;
    private upLoadSvnVerFile;
}
