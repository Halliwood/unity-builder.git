import { CmdOption } from "../CmdOption.js";
declare const enum EFormatErr {
    RichFormatSlashPairErr = "\u5BCC\u6587\u672C#\u914D\u5BF9\u9519\u8BEF",
    HtmlFormatErr = "html\u683C\u5F0F\u9519\u8BEF",
    RichFormatErr = "\u5BCC\u6587\u672C\u683C\u5F0F\u9519\u8BEF"
}
declare interface IFormatErr {
    local: string;
    errors: EFormatErr[];
}
declare interface IMissedFormats {
    local: string;
    fmts: string[];
}
declare interface IReport {
    noLocals: string[];
    missedFmts: IMissedFormats[];
    fmtErrors: IFormatErr[];
    termCNs: string[];
    termENs: string[];
}
declare interface IReportDoc {
    noTranslators: string[];
    concatStrErrors: string[];
    jsonSafeErrors: string[];
    langs: Record<string, IReport>;
}
export declare interface I18NOut {
    localizeParams: string[];
    outputRoot: string;
    newOutput: boolean;
    autoTransFailedCnt: number;
    hasError: boolean;
    report?: IReportDoc;
}
export declare class I18N {
    prepareI18n(): Promise<void>;
    runI18N(cmdOption: CmdOption, mode: 'S' | 'R'): Promise<I18NOut | null>;
}
export {};
