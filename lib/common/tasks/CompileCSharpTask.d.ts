import { BaseTask, TaskResult } from "./BastTask.js";
export declare class CompileCSharpTask extends BaseTask {
    run(): Promise<TaskResult<void>>;
}
