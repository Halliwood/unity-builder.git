import { BaseTask, TaskResult } from "../../common/tasks/BastTask.js";
export declare class LayaPreBuildTask extends BaseTask<void> {
    run(): Promise<TaskResult<void>>;
    private readPropertiesFromTs;
}
