export class CommonEnv {
    public static readonly PublishAssets = 'publish/assets';

    public static readonly EditorBuildSettingsAsset = 'ProjectSettings/EditorBuildSettings.asset';
    public static readonly TsSrc = 'TsScripts';
    public static readonly TsOutput = 'TsScripts/.dist';
    public static readonly PuertsWebGLJSLib = 'Assets/Puerts_webgl/Runtime/Plugins/WebGL';
    public static readonly PuertsDLLMockLibJS = 'Assets/Puerts_webgl/Javascripts~/PuertsDLLMock';
    public static readonly PuertsJSLib = 'Editor/Resources/puerts';
    public static readonly PuerGen = 'Assets/Gen';
    public static readonly PuertsWebglRoot = 'Assets/Puerts_webgl';

    public static readonly I18N_TS = 'TsScripts/System/i18n/i18n.ts';
    public static readonly I18N_CS = 'Assets/Scripts/i18n/i18n.cs';
    public static readonly RESLOADER_CS = 'Assets/Scripts/game/AssetLoader/ResLoader.cs';
    public static readonly Root_Unity = 'Assets/Scenes/root.unity';
    public static readonly Root_webgl_Unity = 'Assets/Scenes/root_webgl.unity';

    public static readonly ComLib = 'libs/cjs/com';

    public static readonly VersionCodeTxt = 'build/versionCode.txt';
}
