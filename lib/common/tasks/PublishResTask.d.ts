import { BaseTask, TaskResult } from './BastTask.js';
export declare class PublishResTask extends BaseTask {
    /**下面这些文件类型的文件都是唯一的，忽略对它们的md5的比较*/
    private readonly ingorehashexts;
    run(): Promise<TaskResult<void>>;
    /**
     * 除了txt外，其他都以文件名为准，因为我们的文件名都是唯一的
     */
    private diffLastVer;
    private upLoadRes;
    get skip(): boolean;
}
