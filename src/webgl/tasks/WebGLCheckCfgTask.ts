import { BaseTask, TaskResult } from "../../common/tasks/BastTask.js";
import { toolchain } from '../../toolchain.js';

/**检查cfg.json */
export class WebGLCheckCfgTask extends BaseTask<void> {
    async run(): Promise<TaskResult<void>> {
        if (toolchain.params.projGlobalCfg == null || toolchain.params.projGlobalCfg.support_platforms == null) {
            // 发布webgl时如果项目还包含pc、安卓等平台，则需要重新wrap已减少wrap代码，因此需确保cfg.json为新版配置且包含相关必须字段
            return { success: false, errorCode: 1, message: 'cfg.json未包含global.support_platforms字段，请修改！', data: void 0 };
        }
        if (this.cmdOption.webglRuntime == 'minigame' && toolchain.params.channelCfg.wx == null) {
            // 发布微信小游戏需要配置appid
            return { success: false, errorCode: 1, message: 'cfg.json需配置wx.appid！', data: void 0 };
        }
        if (this.cmdOption.webglRuntime == 'douyin' && toolchain.params.channelCfg.douyin == null) {
            // 发布抖音小游戏需要配置appid
            return { success: false, errorCode: 1, message: 'cfg.json需配置douyin.appid！', data: void 0 };
        }
        return { success: true, errorCode: 0, data: void 0 };
    }
}
