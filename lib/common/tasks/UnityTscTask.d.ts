import { BaseTask, TaskResult } from "../../common/tasks/BastTask.js";
/**Unity编译ts */
export declare class UnityTscTask extends BaseTask<void> {
    run(): Promise<TaskResult<void>>;
}
