export class TaskHelper {
    taskResults = {};
    saveResult(name, result) {
        this.taskResults[name] = result;
    }
    getResult(name) {
        return this.taskResults[name];
    }
}
//# sourceMappingURL=TaskHelper.js.map