import { BaseTask, TaskResult } from "./BastTask.js";
export declare class CheckEnvTask extends BaseTask {
    run(): Promise<TaskResult<void>>;
}
