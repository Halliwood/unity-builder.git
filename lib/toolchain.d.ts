import { SVNClient } from "@taiyosen/easy-svn";
import { BuildParams } from "./BuildParams.js";
import { HostInfoParser } from "./cfg/HostInfoParser.js";
import { PlatformCfgParser } from "./cfg/PlatformCfgParser.js";
import { SvnCfgParser } from "./cfg/SvnCfgParser.js";
import { CmdOption } from "./CmdOption.js";
import { I18N } from "./common/I18N.js";
import { HtmlMaker } from "./HtmlMaker.js";
import { LogParser } from "./LogParser.js";
import { TaskHelper } from "./TaskHelper.js";
import { FilterPuertsReserved } from "./tools/FilterPuertsReserved.js";
import { JsonBuilder } from "./tools/JsonBuilder.js";
import { ILockFile } from "./tools/lock.js";
import { UnityHelper } from "./tools/UnityHelper.js";
import { ProjIniCfgParser } from "./cfg/ProjIniCfgParser.js";
export declare interface IBuildInfo {
    /**本次构建的资源版本号，请确保在执行BuildAssetsTask后方可访问本字段 */
    resVersion?: number;
    /**本次构建的json版本号，请确保在执行JsonBundleTask后方可访问本字段 */
    jsonVersion?: string;
}
export declare const toolchain: {
    __dirname: string;
    startAt: number;
    startByTimer: boolean;
    startUser: string;
    lockfile: ILockFile | null;
    buildMessages: string[];
    appendLogURL: boolean;
    hostInfoParser: HostInfoParser;
    svnCfgParser: SvnCfgParser;
    platformCfgParser: PlatformCfgParser;
    projIniCfgParser: ProjIniCfgParser;
    option: CmdOption;
    params: BuildParams;
    svnClient: SVNClient;
    buildInfo: IBuildInfo;
    i18n: I18N;
    unity: UnityHelper;
    logParser: LogParser;
    jsonBuilder: JsonBuilder;
    htmlMaker: HtmlMaker;
    taskHelper: TaskHelper;
    filterPuertsReserved: FilterPuertsReserved;
    wechatDevtools: string;
};
