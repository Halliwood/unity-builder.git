import { BaseTask, TaskResult } from "./BastTask.js";
export declare class ReadCfgTask extends BaseTask {
    run(): Promise<TaskResult<void>>;
    get executeAlways(): boolean;
}
