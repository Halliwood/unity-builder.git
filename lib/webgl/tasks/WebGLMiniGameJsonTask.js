import fg from "fast-glob";
import fs from "fs-extra";
import { BaseTask } from "../../common/tasks/BastTask.js";
import { toolchain } from '../../toolchain.js';
import path from "path";
import { md5, readVersionTxt } from "../../tools/vendor.js";
import { WebGLEnv } from "../WebGLEnv.js";
/**小游戏json不打ab包，直接上传到cdn */
export class WebGLMiniGameJsonTask extends BaseTask {
    async run() {
        const jsonInsteadOfAB = toolchain.params.channelCfg.wx?.jsonInsteadOfAB;
        if (jsonInsteadOfAB) {
            // 需要主动配置使用原始json而非ab包才生效
            const jsonRoot = path.join(toolchain.params.workSpacePath, 'Assets/AssetSources/data');
            let jsons = ['bundle.json'];
            if (jsonInsteadOfAB == 'individual') {
                jsons = await fg('*.json', { cwd: jsonRoot, ignore: ['bundle.json'] });
            }
            const assetRoot = path.join(toolchain.params.uploadPath, WebGLEnv.MiniGameAssetRoot);
            const dstRoot = path.join(assetRoot, 'data');
            await fs.ensureDir(dstRoot);
            const md5Book = {};
            for (const json of jsons) {
                const jsonFile = path.join(jsonRoot, json);
                const content = await fs.readFile(jsonFile);
                const fileMd5 = md5(content).substring(0, 10);
                const fileName = path.basename(json, '.json');
                md5Book[fileName] = fileMd5;
                await fs.copyFile(jsonFile, path.join(dstRoot, fileName + '_' + fileMd5 + '.json'));
            }
            // 输出json版本信息到version.txt
            const versionTxt = path.join(assetRoot, 'version.txt');
            const [resVer] = await readVersionTxt(versionTxt);
            // 修改version.txt，内容分两行，第一行为资源版本号，第二行为json版本信息
            const newVcontent = resVer + '\n' + (jsonInsteadOfAB == 'bundle' ? md5Book['bundle'] : JSON.stringify(md5Book));
            console.log('write to version.txt: ');
            console.log(newVcontent);
            await fs.writeFile(versionTxt, newVcontent, 'utf-8');
        }
        else {
            console.log('skip');
        }
        return { success: true, errorCode: 0, data: void 0 };
    }
}
//# sourceMappingURL=WebGLMiniGameJsonTask.js.map