/**
 * 默认环境变量。新版unity-builder不再需要配置hostinfo.yml，并通过各项目的xxx.ini结合下述环境变量自动生成构建配置。
 * 如需修改默认环境变量，你可以在Hudson或jenkins后台设置下述环境变量。
 */
export const env = {
    projIniRoot: 'D:/game_configs',
    defaultWorkspace: 'F:/games',
    defaultResRoot: 'D:/res',
    defaultNDK: 'D:/android-ndk-windows',
    /**MAC设备信息，格式：IP:端口@用户名,密码:工作目录 */
    defaultMac: '192.168.9.105:22@fygame,teppei:/Users/fygame/Documents/pcworkspace/',
    ChatSvr: 'http://localhost:3000',
    clientGroupChatID: 'client',
    clientGroupMention: 'bill,metrofee',
    svnUser: '',
    svnPswd: '',
    ttMail: '',
    ttPswd: ''
};

export const fillEnvValues = () => {
    env.projIniRoot = process.env.PROJ_INI_ROOT || env.projIniRoot;
    env.defaultWorkspace = process.env.DEFAULT_WORKSPACE || env.defaultWorkspace;
    env.defaultResRoot = process.env.DEFAULT_RES_ROOT || env.defaultResRoot;
    env.defaultNDK = process.env.NDK_ROOT || env.defaultNDK;
    env.defaultMac = process.env.DEFAULT_MAC_DEVICE || env.defaultMac;
    env.ChatSvr = process.env.CHAT_SVR || env.ChatSvr;
    env.clientGroupChatID = process.env.CLIENT_GROUP_CHATID || env.clientGroupChatID;
    env.clientGroupMention = process.env.CLIENT_GROUP_MENTION || env.clientGroupMention;
    env.svnUser = process.env.SVN_USR!;
	env.svnPswd = process.env.SVN_PSWD!;
    env.ttMail = process.env.TT_MAIL!;
    env.ttPswd = process.env.TT_PSWD!;
}
