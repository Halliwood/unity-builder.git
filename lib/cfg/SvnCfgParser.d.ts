import { BuildParams } from '../BuildParams.js';
import { CmdOption } from '../CmdOption.js';
export declare class SvnCfgParser {
    getSVNCfg(option: CmdOption, params: BuildParams): Promise<void>;
}
