import { PkgType } from "../../typings";
import { BaseTask, TaskResult } from "./BastTask.js";
export declare abstract class BaseMakeHtmlTask extends BaseTask {
    protected makeHtml(pkgType: PkgType, patterns: string[]): Promise<TaskResult<void>>;
}
