import { Builder } from "../Builder.js";
import { PreBuildAppTask } from "../common/tasks/PreBuildAppTask.js";
import { BuildAssetsTask } from "../common/tasks/BuildAssetsTask.js";
import { IosBuildUnityTask } from "./tasks/IosBuildUnityTask.js";
import { IosMakeHtmlTask } from "./tasks/IosMakeHtmlTask.js";
import { I18NTask } from "../common/tasks/I18NTask.js";
import { UnityTscTask } from "../common/tasks/UnityTscTask.js";
export class IosBuilder extends Builder {
    cmdOption;
    constructor(cmdOption) {
        super(cmdOption);
        this.cmdOption = cmdOption;
        this.series.push(new I18NTask(this.cmdOption), new UnityTscTask(this.cmdOption), new BuildAssetsTask(this.cmdOption), new PreBuildAppTask(this.cmdOption), new IosBuildUnityTask(this.cmdOption), new IosMakeHtmlTask(this.cmdOption));
    }
}
//# sourceMappingURL=IosBuilder.js.map