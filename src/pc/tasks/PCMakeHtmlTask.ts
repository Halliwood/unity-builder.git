import { BaseMakeHtmlTask } from "../../common/tasks/BaseMakeHtmlTask.js";
import { TaskResult } from "../../common/tasks/BastTask.js";

export class PCMakeHtmlTask extends BaseMakeHtmlTask {
    async run(): Promise<TaskResult<void>> {
        return await this.makeHtml('exe', ['*.zip']);
    }
}
