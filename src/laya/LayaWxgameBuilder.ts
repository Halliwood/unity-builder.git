import { CmdOption } from "../CmdOption.js";
import { LayaBaseBuilder } from "./LayaBaseBuilder.js";
import { LayaBuildPatchTask } from "./tasks/LayaBuildPatchTask.js";
import { LayaCompileTask } from "./tasks/LayaCompileTask.js";
import { LayaPostPublishWxgameTask } from "./tasks/LayaPostPublishWxgameTask.js";
import { LayaPublishTask } from "./tasks/LayaPublishTask.js";
import { LayaUploadResTask } from "./tasks/LayaUploadResTask.js";
import { LayaVersionedTask } from "./tasks/LayaVersionedTask.js";

export class LayaWxgameBuilder extends LayaBaseBuilder {

    constructor(protected cmdOption: CmdOption) {
        super(cmdOption);

        this.series.push(
            new LayaCompileTask(this.cmdOption),
            new LayaPublishTask(this.cmdOption),
            new LayaVersionedTask(this.cmdOption),
            new LayaBuildPatchTask(this.cmdOption),
            new LayaPostPublishWxgameTask(this.cmdOption),
            new LayaUploadResTask(this.cmdOption),
        );
    }
}
