import { Builder } from "../Builder.js";
import { CmdOption } from "../CmdOption.js";
/**
 * WebGL浏览器构建器
 */
export declare class WebGLBrowserBuilder extends Builder {
    protected cmdOption: CmdOption;
    constructor(cmdOption: CmdOption);
}
