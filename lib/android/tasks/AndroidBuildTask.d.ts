import { BaseTask, TaskResult } from "../../common/tasks/BastTask.js";
import { IBuildAppResult, Nullable } from "../../typings";
export declare class AndroidBuildTask extends BaseTask<IBuildAppResult> {
    run(): Promise<TaskResult<IBuildAppResult>>;
    get dependencies(): Nullable<string[]>;
    get skip(): boolean;
}
