import { BaseTask, TaskResult } from "./BastTask.js";
export declare class TsObfuscateTask extends BaseTask {
    run(): Promise<TaskResult<void>>;
}
