import fs from "fs-extra";
import { toolchain } from "./toolchain.js";

export async function incVersionCode(versionTxt: string): Promise<string> {
    await toolchain.svnClient.update(versionTxt);
    let versionCode = fs.readFileSync(versionTxt, 'utf-8');
    versionCode = String(Number(versionCode) + 1);
    fs.writeFileSync(versionTxt, versionCode, 'utf-8');
    await toolchain.svnClient.commit('auto commit versionCode', versionTxt);
    return versionCode;
}

export async function ignoreSVN(wcRoot: string, ignoreList: string[]): Promise<void> {
    const changed = await toolchain.svnClient.ignore(wcRoot, ...ignoreList);
    if (changed) {
        await toolchain.svnClient.update(wcRoot, '--depth', 'empty');
        await toolchain.svnClient.commit('auto ignore@unity-builder', wcRoot, '--depth', 'empty');
        console.log('ignore success:', ignoreList);
    }
}
