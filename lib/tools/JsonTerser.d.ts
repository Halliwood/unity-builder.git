export declare class JsonTerser {
    private readonly ignoreJsons;
    private keyCntMap;
    private cache;
    run(workSpacePath: string): Promise<void>;
    /**读取保护字段配置 */
    private readReserveds;
    private findChildInSource;
    private obfusecateTs;
    private statKeys;
    private statObjKeys;
    private obfusecateJson;
}
