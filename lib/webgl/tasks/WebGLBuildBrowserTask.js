import { WebGLBuildUnityBaseTask } from "./WebGLBuildUnityBaseTask.js";
import { WebGLEnv } from "../WebGLEnv.js";
export class WebGLBuildBrowserTask extends WebGLBuildUnityBaseTask {
    async run() {
        // 通过微信提供的webgl转化项目进行WebGL Build
        return this.callUnity(WebGLEnv.ExportbrowserRoot, 'BuildForWebGL');
    }
}
//# sourceMappingURL=WebGLBuildBrowserTask.js.map