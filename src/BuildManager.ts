import { Ancestor } from "./Ancestor.js";
import { WebGLBrowserBuilder } from "./webgl/WebGLBrowserBuilder.js";
import { Cleaner } from "./Cleaner.js";
import { CmdOption } from "./CmdOption.js";
import { HudsonJob } from "./HudsonJob.js";
import { IosBuilder } from "./ios/IosBuilder.js";
import { PCBuilder } from "./pc/PCBuilder.js";
import { Servant } from "./Servant.js";
import { WebGLMiniGameBuilder } from "./webgl/WebGLMiniGameBuilder.js";
import { WebGLAllBuilder } from "./webgl/WebGLAllBuilder.js";
import { LayaWxgameBuilder } from "./laya/LayaWxgameBuilder.js";
import { LayaWebBuilder } from "./laya/LayaWebBuilder.js";
import { toolchain } from "./toolchain.js";
import { Hudson } from "./tools/hudson.js";
import { sendBuildSuccess, sendRobotMsg } from "./tools/alert.js";
import { AndroidBuilder } from "./android/AndroidBuilder.js";
import { WebGLDouYinBuilder } from "./webgl/WebGLDouyinBuilder.js";

export class BuildManager {
    private executor?: Ancestor;

    constructor(protected cmdOption: CmdOption) {
    }

    async start() {
        // 获取是谁构建的
        const user = await Hudson.getStartUser();
        if (user === '~') {
            toolchain.startByTimer = true;
        } else {
            toolchain.startUser = user;
        }

        if (this.cmdOption.task) {
            if (this.cmdOption.task === 'Clean') {
                this.executor = new Cleaner(this.cmdOption);
            } else if (this.cmdOption.task === 'hudsonJob') {
                this.executor = new HudsonJob(this.cmdOption);
            } else {
                this.executor = new Servant(this.cmdOption);
            }
        }
        else {
            if (this.cmdOption.engine == 'laya') {
                if (this.cmdOption.platform == 'web') {
                    this.executor = new LayaWebBuilder(this.cmdOption);
                } else if (this.cmdOption.platform == 'Android') {
                    this.executor = new AndroidBuilder(this.cmdOption);
                } else {
                    this.executor = new LayaWxgameBuilder(this.cmdOption);
                }
            } else {
                if(this.cmdOption.platform == 'Windows') {
                    this.executor = new PCBuilder(this.cmdOption);
                } else if (this.cmdOption.platform == 'Android') {
                    this.executor = new AndroidBuilder(this.cmdOption);
                } else if (this.cmdOption.platform == 'iOS') {
                    this.executor = new IosBuilder(this.cmdOption);
                } else if (this.cmdOption.platform == 'WebGL') {
                    if (this.cmdOption.webglRuntime == 'browser') {
                        this.executor = new WebGLBrowserBuilder(this.cmdOption);
                    } else if (this.cmdOption.webglRuntime == 'minigame') {
                        this.executor = new WebGLMiniGameBuilder(this.cmdOption);
                    } else if (this.cmdOption.webglRuntime == 'douyin') {
                        this.executor = new WebGLDouYinBuilder(this.cmdOption);
                    } else {
                        this.executor = new WebGLAllBuilder(this.cmdOption);
                    }
                }
            }
        }
        if (this.executor == null) {
            console.error('Task not supported!');
            process.exit(1);
        }
        console.log(`[unity-builder] ${this.executor.constructor.name} works for you.`);
        const rst = await this.executor.awake();
        if (!rst) {
            process.exit(1);
        }
        await this.executor.start();
        const conclusion = await this.executor.getBuildConclusion();
        if (conclusion) {
            await sendBuildSuccess(conclusion);
        }

        const msgs = await this.executor.getAdditionalMessages();
        for (const msg of msgs) {
            if (msg.type == 'file') {
                await sendRobotMsg(msg.type, msg.file);
            } else {
                await sendRobotMsg(msg.type, msg.msg, msg.mention);
            }
        }

        process.exit(0);
    }
}
