import path from "path";
export class WebGLEnv {
    /**支持贴图压缩的unity最高版本 */
    static CompressTexMaxVer = '2022.3.14';
    static MD5Len = 6;
    static MangleBak = '../mangle_bak';
    static MangleReserved = '../mangle/manglereserved.txt';
    static MangleCache = '../mangle/manglecached.txt';
    static PuertsWebgl = 'Assets/Puerts_webgl';
    static PuertsWebglJSbuild = 'Assets/Puerts_webgl_jsbuild';
    static MiniGameConfigAsset = 'Assets/WX-WASM-SDK/Editor/MiniGameConfig.asset';
    static MiniGameConfigAssetV2 = 'Assets/WX-WASM-SDK-V2/Editor/MiniGameConfig.asset';
    static ExportbrowserRoot = '../export_browser';
    static ExportBuildRoot = 'Build';
    static SvrBrowserRoot = 'browser';
    static ExportMiniGameRoot = '../export_minigame';
    static MiniGameJsResources = 'puerts_minigame_js_resources';
    static MiniGameRoot = 'minigame';
    static MiniGameJs = 'game.js';
    static WebglRoot = 'webgl';
    static WebglCompressedOut = 'webgl-min';
    static MiniGameAssetRoot = 'assets/wx';
    static indexHtml = 'index.html';
    static PuertsRuntimeJs = 'puerts-runtime.js';
    static PuertsBrowserJsResourcesJs = 'puerts_browser_js_resources.js';
    static MiniGameAppVerFile = path.join(WebGLEnv.ExportMiniGameRoot, 'appVer.txt');
    static MiniGameResVerFile = path.join(WebGLEnv.ExportMiniGameRoot, 'resVer.txt');
    static MiniGameQrcode = path.join(WebGLEnv.ExportMiniGameRoot, 'qrcode.base64');
    static MiniGamePreviewOutput = path.join(WebGLEnv.ExportMiniGameRoot, 'preview.json');
    static MiniGameUploadOutput = path.join(WebGLEnv.ExportMiniGameRoot, 'upload.json');
    static ExportDouyinRoot = '../export_douyin';
    static DouyinJsRoot = 'Assets/StreamingAssets/__cp_js_files';
    static DouyinAppVerFile = path.join(WebGLEnv.ExportDouyinRoot, 'appVer.txt');
}
//# sourceMappingURL=WebGLEnv.js.map