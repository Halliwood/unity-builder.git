import { BuildParams } from "./BuildParams.js";
import { CmdOption } from "./CmdOption.js";
import { PlatformCfgParser } from './cfg/PlatformCfgParser.js';
import { PkgType } from './typings';
export declare class HtmlMaker {
    private readonly indexhtml;
    private readonly apkitem;
    private readonly listhtml;
    private readonly testGameId;
    /**记录apk url */
    readonly apkUrlMapper: Record<string, string>;
    exportPkgsListHtml(apkpath: string, option: CmdOption, params: BuildParams, platCfgParser: PlatformCfgParser, pkgType: PkgType, patterns: string[]): Promise<void>;
    private _exportOneChannelApks;
    private _getApks;
}
