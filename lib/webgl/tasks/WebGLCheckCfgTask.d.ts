import { BaseTask, TaskResult } from "../../common/tasks/BastTask.js";
/**检查cfg.json */
export declare class WebGLCheckCfgTask extends BaseTask<void> {
    run(): Promise<TaskResult<void>>;
}
