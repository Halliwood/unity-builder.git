import fg from "fast-glob";
import { BaseTask } from "../../common/tasks/BastTask.js";
import { layatoolchain } from "../../layatoolchain.js";
import { Nullable } from "../../typings.js";
import { LayaPreBuildTask } from "./LayaPreBuildTask.js";

export abstract class LayaPostPublishBaseTask extends BaseTask<void> {
    async getVersionCode(): Promise<string> {
        // 获取version.min.json的md5
        const vermin = await fg('version.min*.json', { cwd: layatoolchain.vars.publishRoot });
        if (vermin.length == 0) {
            return '';
        }
        // 写入verMD5到game.js
        const versionCode = vermin[0].substring('version.min_'.length, vermin[0].length - '.json'.length);
        console.log('versionCode:', versionCode);
        return versionCode;
    }

    get dependencies(): Nullable<string[]> {
        return [LayaPreBuildTask.name];
    }
}
