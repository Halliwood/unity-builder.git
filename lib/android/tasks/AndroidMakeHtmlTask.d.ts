import { BaseMakeHtmlTask } from "../../common/tasks/BaseMakeHtmlTask.js";
import { TaskResult } from "../../common/tasks/BastTask.js";
export declare class AndroidMakeHtmlTask extends BaseMakeHtmlTask {
    run(): Promise<TaskResult<void>>;
}
