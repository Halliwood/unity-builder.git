export declare const enum EErrorType {
    AssetsUnrecognized = "Assets Unrecognized",
    InvalidFileName = "Invalid file name",
    CompressAtlasNonSquare = "CompressAtlas non square",
    IncorrectSceneFile = "Incorrect scene file",
    CompressTextureFailed = "Compress texture failed",
    CompilationFailed = "Compilation failed",
    NodeFailed = "Node failed",
    CreateABFailed = "CreateAB failed",
    AddABFailed = "Add assetbuddle failed",
    Exception = "Exception",
    Other = "Other failed"
}
export interface IErrorFile {
    file: string;
    errorCode: string;
    errorMessage: string;
}
export declare class LogParser {
    private readonly __excludes;
    private __error;
    get error(): string | null;
    private __errorFiles;
    get errorFiles(): IErrorFile[];
    private __tag;
    isKindOf(type: EErrorType): boolean;
    getUnrecognizedAssets(logstr: string): string[];
    private _getUnrecognizedAssetsPatt;
    private _printAssetsUnrecognized;
    parseUnityBuildLog(logstr: string, exitCode: number): void;
    parseTscLog(logstr: string): void;
    private _printInvalidFileName;
    private _printCompressNonSquare;
    private _printIncorrectSceneFile;
    private _printNotSupportedCompressed;
    private _printCompileFailed;
    private _printNodeFailed;
    private _printBuildABFailed;
    private _printAddAssetBuddleFailed;
    private _printException;
    private _printOtherFailed;
    private _printError;
    private _exclude_log;
}
