import { Builder } from "../Builder.js";
import { WebGLPostBuildBrowserTask } from "./tasks/WebGLPostBuildBrowserTask.js";
import { WebGLBuildBrowserTask } from "./tasks/WebGLBuildBrowserTask.js";
import { WebGLInstallTask } from "./tasks/WebGLInstallTask.js";
import { BuildAssetsTask } from "../common/tasks/BuildAssetsTask.js";
import { PreBuildAppTask } from "../common/tasks/PreBuildAppTask.js";
import { WebGLCheckCfgTask } from "./tasks/WebGLCheckCfgTask.js";
import { I18NTask } from "../common/tasks/I18NTask.js";
import { JsonBundleTask } from "../common/tasks/JsonBundleTask.js";
import { UnityTscTask } from "../common/tasks/UnityTscTask.js";
/**
 * WebGL浏览器构建器
 */
export class WebGLBrowserBuilder extends Builder {
    cmdOption;
    constructor(cmdOption) {
        super(cmdOption);
        this.cmdOption = cmdOption;
        this.series.push(new WebGLCheckCfgTask(this.cmdOption), new WebGLInstallTask(this.cmdOption), new I18NTask(this.cmdOption), new UnityTscTask(this.cmdOption), new BuildAssetsTask(this.cmdOption), new PreBuildAppTask(this.cmdOption), new JsonBundleTask(this.cmdOption), new WebGLBuildBrowserTask(this.cmdOption), new WebGLPostBuildBrowserTask(this.cmdOption));
    }
}
//# sourceMappingURL=WebGLBrowserBuilder.js.map