export declare class Mangler {
    private readonly alwaysReserved;
    private reserved;
    private nameCache;
    private minifyOption;
    beginMangle(reserved: string[], nameCacheFile: string, keepClassNames: boolean, mangleProperties: boolean): Promise<void>;
    endMangle(nameCacheFile: string): Promise<void>;
    mangleAll(inputDir: string, outputDir: string, includeExt: '.js' | '.json'): Promise<void>;
    mangleOneJs(inputFile: string, outputFile: string): Promise<void>;
    mangleOneJson(inputFile: string, outputFile: string): Promise<void>;
    patchJs(dir: string): Promise<void>;
    private patchAllJsInternal;
}
