export declare const enum EFileErrorType {
    CodeError = 1,
    FileNameError = 2
}
export declare function alertErrorFile(file: string, type: EFileErrorType, msg?: string): Promise<string>;
export declare function sendBuildFailureAlert(msg: string, printMsg?: boolean, suggestRetry?: boolean): Promise<void>;
export declare function sendBuildSuccess(conclusion: string): Promise<void>;
export declare function sendUserChatText(touser: string, content: string): Promise<void>;
export declare function sendRobotImage(base64: string, chatid?: string): Promise<void>;
export declare function sendRobotMsg(type: 'text' | 'markdown' | 'file', msg: string, mention?: string, chatid?: string): Promise<void>;
export declare function sendVIPAD(): Promise<void>;
/**
 * 上传文件
 * @param file 注意空文件上传将失败
 * @returns
 */
export declare function uploadMedia(file: string): Promise<string | null>;
