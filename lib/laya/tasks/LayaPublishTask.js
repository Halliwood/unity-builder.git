import fs from "fs-extra";
import path from "path";
import { BaseTask } from "../../common/tasks/BastTask.js";
import { layatoolchain } from "../../layatoolchain.js";
import { toolchain } from '../../toolchain.js';
import { Cmd } from '../../tools/Cmd.js';
/**Laya导出工程 */
export class LayaPublishTask extends BaseTask {
    async run() {
        process.chdir(toolchain.params.workSpacePath);
        await fs.emptyDir(layatoolchain.vars.publishRoot);
        // 读取所需node版本
        const rcFile = path.join(toolchain.params.workSpacePath, '.nvmrc');
        if (fs.existsSync(rcFile)) {
            const nvmrc = await fs.readFile(rcFile, 'utf-8');
            await new Cmd().run('nvs.cmd', ['exec', nvmrc, 'layaair2-cmd.cmd', 'publish', '-c', this.cmdOption.platform]);
        }
        else {
            await new Cmd().runNodeModule('layaair2-cmd', ['publish', '-c', this.cmdOption.platform]);
        }
        return { success: true, errorCode: 0, data: void 0 };
    }
}
//# sourceMappingURL=LayaPublishTask.js.map