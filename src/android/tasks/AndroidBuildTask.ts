import { BaseTask, TaskResult } from "../../common/tasks/BastTask.js";
import { PreBuildAppTask } from "../../common/tasks/PreBuildAppTask.js";
import { WrapCSharpTask } from "../../common/tasks/WrapCSharpTask.js";
import { IBuildAppResult, Nullable } from "../../typings";
import { AndroidBuildLaya } from "./AndroidBuildLaya.js";
import { AndroidBuildUnity } from "./AndroidBuildUnity.js";


export class AndroidBuildTask extends BaseTask<IBuildAppResult> {
    async run(): Promise<TaskResult<IBuildAppResult>> {
        if (this.cmdOption.engine == "laya") {
            return await AndroidBuildLaya.run(this.cmdOption);
        } else {
            return await AndroidBuildUnity.run(this.cmdOption);
        }
    }

    get dependencies(): Nullable<string[]> {
        if (this.cmdOption.engine == "laya") {
            return [];
        } else {
            return [WrapCSharpTask.name, PreBuildAppTask.name];;
        }
    }

    get skip(): boolean {
        return !this.cmdOption.buildExe;
    }
}