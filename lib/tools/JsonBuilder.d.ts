import { SVNClient } from '@taiyosen/easy-svn';
import { BuildParams } from '../BuildParams.js';
import { LogParser } from '../LogParser.js';
export declare class JsonBuilder {
    build_bjson(exePath: string, buildParams: BuildParams, svnClient: SVNClient, logParser: LogParser): Promise<void>;
}
