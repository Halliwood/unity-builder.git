import { BaseTask, TaskResult } from "../../common/tasks/BastTask.js";
export declare class LayaBuildPatchTask extends BaseTask<void> {
    run(): Promise<TaskResult<void>>;
}
