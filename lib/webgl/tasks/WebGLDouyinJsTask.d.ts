import { TaskResult } from "../../common/tasks/BastTask.js";
import { WebGLProcessJsTask } from "./WebGLPostBuildUnityTask.js";
export declare class WebGLDouyinJsTask extends WebGLProcessJsTask {
    run(): Promise<TaskResult<void>>;
}
