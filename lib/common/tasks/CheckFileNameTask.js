import { toolchain } from "../../toolchain.js";
import { BaseTask } from "./BastTask.js";
import fg from "fast-glob";
import path from "path";
import { alertErrorFile, sendBuildFailureAlert } from "../../tools/alert.js";
export class CheckFileNameTask extends BaseTask {
    async run() {
        if (toolchain.option.platform == 'WebGL') {
            const assetsRoot = path.join(toolchain.params.workSpacePath, 'Assets');
            const images = await fg(['**/*.png', '**/*.jpg', '**/*.tga'], { cwd: assetsRoot });
            const repeatDots = [], illegalTokens = [], hans = [];
            for (const img of images) {
                if (img.includes('..')) {
                    // 小游戏贴图文件如果包含多余的.会导致加载失败
                    repeatDots.push(img);
                }
                else if (img.includes('images/sprite') || img.includes('ui/texture')) {
                    // 非图集图片文件名不允许有空格和中文，否则会加载失败
                    if (img.includes(' ')) {
                        illegalTokens.push(img);
                    }
                    else if (img.search(/[\u4e00-\u9fa5]+/) >= 0) {
                        hans.push(img);
                    }
                }
            }
            const msgs = [];
            let errorFile = '', fileStr = '';
            if (repeatDots.length > 0) {
                fileStr = repeatDots.map(v => path.basename(v)).join(', ');
                msgs.push(`\`多余的.(${repeatDots.length})\`: ${fileStr}`);
                errorFile = repeatDots[0];
            }
            if (illegalTokens.length > 0) {
                fileStr = illegalTokens.map(v => path.basename(v)).join(', ');
                msgs.push(`\`包含空格(${illegalTokens.length})\`: ${fileStr}`);
                errorFile = illegalTokens[0];
            }
            if (hans.length > 0) {
                fileStr = hans.map(v => path.basename(v)).join(', ');
                msgs.push(`\`包含中文(${hans.length})\`: ${fileStr}`);
                errorFile = hans[0];
            }
            if (msgs.length > 0) {
                await alertErrorFile(path.join(assetsRoot, errorFile), 2 /* EFileErrorType.FileNameError */, fileStr);
                await sendBuildFailureAlert('以下资源命名错误：' + msgs.join('\n'));
                process.exit(1);
            }
        }
        return { success: true, errorCode: 0 };
    }
}
//# sourceMappingURL=CheckFileNameTask.js.map