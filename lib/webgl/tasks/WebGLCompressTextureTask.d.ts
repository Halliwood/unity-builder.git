import { BaseTask, TaskResult } from "../../common/tasks/BastTask.js";
export interface WebGLCompressResult {
    compressed: boolean;
    guidence?: string;
}
export declare class WebGLCompressTextureTask extends BaseTask<WebGLCompressResult> {
    run(): Promise<TaskResult<WebGLCompressResult>>;
    private compressTexture;
    get skip(): boolean;
}
