export declare function incVersionCode(versionTxt: string): Promise<string>;
export declare function ignoreSVN(wcRoot: string, ignoreList: string[]): Promise<void>;
