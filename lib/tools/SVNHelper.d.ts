export declare class SVNHelper {
    static pullPublish(): Promise<void>;
    static getVersion(pathOrUrl: string): Promise<string>;
    private static exists;
    private static back_publish_bytag;
    private static delete_publish;
    private static tag_trunk;
    private static pullpublish_from_backtrunktag;
    private static copy_singlefile_from_backpublishtag;
}
