import { BaseTask, TaskResult } from "./BastTask.js";
/**斗破专用提审任务，其他项目不建议使用 */
export declare class LegacyTiShenTask extends BaseTask {
    run(): Promise<TaskResult<void>>;
}
