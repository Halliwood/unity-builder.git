import { toolchain } from "../../toolchain.js";
import { BaseTask } from "./BastTask.js";
/**
 * 保存本次SVN版本号信息。
 */
export class SetScriptDefineTask extends BaseTask {
    async run() {
        if (toolchain.option.engine == 'unity' && toolchain.option.platform == 'WebGL') {
            let method = null;
            if (toolchain.option.webglRuntime == 'minigame') {
                method = 'SetAsWeixin';
            }
            else if (toolchain.option.webglRuntime) {
                method = 'SetAsDouyin';
            }
            if (method != null) {
                const params = ['-batchmode', '-projectPath', toolchain.params.workSpacePath, '-executeMethod', `BuildCommand.${method}`, '-quit'];
                await toolchain.unity.runUnityCommand(this.cmdOption, params, 'SetScriptDefine', true);
            }
        }
        return { success: true, errorCode: 0 };
    }
}
//# sourceMappingURL=SetScriptDefineTask.js.map