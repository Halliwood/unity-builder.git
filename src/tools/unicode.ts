export function convertToUnicode(str: string): string {
    var res = [];
    for (var i = 0, len = str.length; i < len; i++) {
        res[i] = ("00" + str.charCodeAt(i).toString(16)).slice(-4);
    }
    return "\\u" + res.join("\\u");
}
