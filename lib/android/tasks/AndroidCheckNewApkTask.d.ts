import { BaseTask, TaskResult } from "../../common/tasks/BastTask.js";
export declare type ICheckNewApkResult = {
    needNewApk?: false;
} | {
    needNewApk: true;
    hitPrefab: string;
    hitCS: string;
};
/**
 * unity android检查是否需要换包
 */
export declare class AndroidCheckNewApkTask extends BaseTask<ICheckNewApkResult> {
    run(): Promise<TaskResult<ICheckNewApkResult>>;
}
