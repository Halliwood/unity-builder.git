import { Client } from 'ssh2';
export class SSHClient {
    conn = new Client();
    os;
    cmdStdout = '';
    async connect(hostinfo) {
        const conn = this.conn;
        return new Promise((resolve, reject) => {
            conn.on('ready', () => {
                console.log('ssh connected:', hostinfo.host + ':' + hostinfo.port);
                resolve(0);
            }).on('error', (err) => {
                console.error(err);
                reject(err);
            }).connect(hostinfo);
        });
    }
    async exec(cmd, silent) {
        const conn = this.conn;
        this.cmdStdout = '';
        console.log('$ ' + cmd);
        return new Promise((resolve, reject) => {
            conn.exec(cmd, (err, stream) => {
                if (err) {
                    console.error(err);
                    reject(err);
                }
                else {
                    stream.on('close', () => {
                        resolve(this.cmdStdout);
                    }).on('data', (data) => {
                        if (!silent)
                            console.log(String(data));
                        this.cmdStdout += String(data);
                    }).stderr.on('data', (data) => {
                        console.error('STDERR: ' + data);
                        reject(new Error(data));
                    });
                }
            });
        });
    }
    async put(localPath, remotePath) {
        const conn = this.conn;
        return new Promise((resolve, reject) => {
            conn.sftp((err, sftp) => {
                if (err) {
                    console.error(err);
                    reject(err);
                }
                else {
                    sftp.on('error', (err) => {
                        console.error(err);
                        reject(err);
                    }).fastPut(localPath, remotePath, (err) => {
                        if (err) {
                            console.error(err);
                            reject(err);
                        }
                        else {
                            resolve(0);
                        }
                    });
                }
            });
        });
    }
    async get(remotePath, localPath) {
        const conn = this.conn;
        return new Promise((resolve, reject) => {
            conn.sftp((err, sftp) => {
                if (err) {
                    console.error(err);
                    reject(err);
                }
                else {
                    sftp.on('error', (err) => {
                        console.error(err);
                        reject(err);
                    }).fastGet(remotePath, localPath, (err) => {
                        if (err) {
                            console.error(err);
                            reject(err);
                        }
                        else {
                            console.log(`put ${localPath} finished`);
                            resolve(0);
                        }
                    });
                }
            });
        });
    }
    end() {
        const conn = this.conn;
        return new Promise((resolve, reject) => {
            conn.on('end', () => {
                console.log('ssh ended');
            }).on('close', (hadError) => {
                if (hadError) {
                    console.error('ssh closed with error');
                    resolve(1);
                }
                else {
                    console.log('ssh closed without error');
                    resolve(0);
                }
            }).end();
        });
    }
}
//# sourceMappingURL=SSHClient.js.map