import { BaseTask, TaskResult } from "../../common/tasks/BastTask.js";
/**生成小游戏预加载清单 */
export declare class WebGLMiniGamePreloadTask extends BaseTask<void> {
    run(): Promise<TaskResult<void>>;
}
