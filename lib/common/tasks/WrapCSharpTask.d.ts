import { BaseTask, TaskResult } from "./BastTask.js";
export declare class WrapCSharpTask extends BaseTask {
    run(): Promise<TaskResult<void>>;
    get skip(): boolean;
}
