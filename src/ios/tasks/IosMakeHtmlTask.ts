import { BaseMakeHtmlTask } from "../../common/tasks/BaseMakeHtmlTask.js";
import { TaskResult } from "../../common/tasks/BastTask.js";

export class IosMakeHtmlTask extends BaseMakeHtmlTask {
    async run(): Promise<TaskResult<void>> {
        return await this.makeHtml('ipa', ['*.ipa', '*.aab']);
    }
}
