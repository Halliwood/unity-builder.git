export interface DevtoolsResult<T> {
    code: number;
    data: T;
}
export interface UploadResult {
    success: boolean;
    needLogin: boolean;
    error?: string;
}
export declare class WechatTools {
    static getDevtools(): Promise<void>;
    static islogin(): Promise<DevtoolsResult<boolean>>;
    static login(): Promise<void>;
    static preview(projectRoot: string, qrOuput: string, infoOutput: string): Promise<DevtoolsResult<boolean>>;
    static upload(projectRoot: string, version: string, desc: string, infoOutput: string): Promise<DevtoolsResult<UploadResult>>;
    static canWorkWithCi(projectPath: string): Promise<boolean>;
    static uploadByCi(projectPath: string, sourceMapSavePath: string, version: string, desc: string): Promise<DevtoolsResult<UploadResult>>;
    static quit(projectRoot: string): Promise<void>;
}
