import { BaseTask, TaskResult } from "../../common/tasks/BastTask.js";
import { Nullable } from '../../typings';
export interface BuildUnityResult {
    version: string;
}
export declare abstract class WebGLBuildUnityBaseTask extends BaseTask<BuildUnityResult> {
    callUnity(output: string, buildCommand: 'BuildForWebGL' | 'BuildForMiniGame' | 'BuildForDouyin'): Promise<TaskResult<BuildUnityResult>>;
    get dependencies(): Nullable<string[]>;
    get skip(): boolean;
}
