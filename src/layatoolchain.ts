import path from "path";
import { ProjectType } from "./BuildParams.js";
import { toolchain } from "./toolchain.js";
import { TFileNameMapper, TMd5Map } from "./typings.js";

export declare interface ILayaBuildConfig {
    gameid: number,
    gamename: string,
    platid: number,
    pf: string,
    svrurl: string,
    svrips: string[],
    svripfmt: string,
    resurl: string,
    defines: string,
    version: string,
    fyReportUrl: string,
    uiReplacement?: { [original: string]: string },
    statement: string,
    bottom: number
}

interface LayaVars {
    publishRoot: string,
    buildConfigMap: { [pf: string]: ILayaBuildConfig },
    fileNameMapper: TFileNameMapper,
    md5Map: TMd5Map,
    /**不同平台可能共享同一个CDN，共享一个patch包 */
    patchMap: { [file: string]: string }
}

const vars: LayaVars = { publishRoot: '', buildConfigMap: {}, fileNameMapper: {}, md5Map: {}, patchMap: {} };

export const layatoolchain = {
    vars, 
    commonFileRoot: () => path.join(toolchain.params.workSpacePath, 'build/projectfiles/common'), 
    projectFilesRoot: (pf: string, type: 'common' | ProjectType) => path.join(toolchain.params.workSpacePath, 'build/projectfiles', pf, type),
    newRecRoot: (oldToolsRoot: string) => path.join(oldToolsRoot, 'h5Build/configs', '__new'),
    projPublishRecRoot: (oldToolsRoot: string) => path.join(oldToolsRoot, 'h5Build/configs', toolchain.params.projectName),
    pfPublishRecRoot: (oldToolsRoot: string, pf: string) => path.join(oldToolsRoot, 'h5Build/configs', toolchain.params.projectName, pf, toolchain.params.projectType)
};
